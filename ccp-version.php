<?php
header("Content-Type: text/plain; charset=utf-8");
header("Access-Control-Allow-Origin: http://orteil.dashnet.org");
header("Timing-Allow-Origin: http://orteil.dashnet.org");	// pour timing des demandes (debug)
// header("Access-Control-Request-Method: GET,POST,HEAD");
// header("Access-Control-Allow-Credentials: true");	// pour utiliser les cookies
// header("Access-Control-Max-Age: 3600");	// cache du prefetch en seconde

// retourne le numéro de version en cours
echo 16.0;
// echo json_encode(array('version'=>1.6, 'note'=>'test'));

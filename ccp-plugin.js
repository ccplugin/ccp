/* ================================================

    CCP - A Cookie Clicker plugin

    Version: 16.0
    License: MIT
    Website: https://ccp.vyshorin.fr
    GitLab: https://gitlab.com/ccplugin/ccp
    Authors: Victor Gavoille
             Florent Colinet

================================================ */

"use strict";

function log(str) {
	try {console.log(str);} catch(ex) {}
}
function warn(str) {
	try {console.warn(str);} catch(ex) {}
}
function alert(title, desc, pic, quick) {
	if(!quick) quick = 6;
	if(Game.prefs.popups) Game.Popup(title);
	else Game.Notify(title, desc, pic, quick);
}

function Beautify(value, floats) {
	var negative=(value<0),
		decimal='',
		output='';
	if (value<1000000 && floats>0 && Math.floor(value.toFixed(floats))!=value.toFixed(floats)) decimal='.'+(value.toFixed(floats).toString()).split('.')[1];
	value=Math.floor(Math.abs(value));
	var formatter=numberFormatters[Game.prefs.format?2:1];
	if(value>1e21)	// corrige le problème des nombres du type 1.2,345e21 par 1.234,5e21
		output=formatter(value).toString().replace(/(\d{3}(?=\d))/g,'$1,');
	else
		output=formatter(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g,',');
	return negative?'-'+output:output+decimal;
}

// ------------------------------------

// fonctions pour se brancher proprement sur le jeu
function addChecks(fonction) {
	Game.customChecks.push(fonction);
}

/* function _creerPanel() {		// exemple de chose à mettre dans le menu addon
	var sss = ''
		+ '<div class="title"><span class="icon"></span>Ma sous section</div>'
		+ '<div class="listing"><b>Ma ligne d\'addon :</b> <span id="lalalalala">Je test</span></div>'
		;
	return sss;
} */

// ------------------------------------

// object pour gérer les globales
function CCPGlobals() {
	this.self = null;

	this.game = l('game');

	this.rows = l('rows');
	this.menu = l('menu');
	this.gameButtons = [l('statsButton'), l('prefsButton'), l('logButton')];

	// this.orderBase = 1000000;

	this.CCPPanel = null;
	this.CCPPanelButton = null;

	this.cache = {
		'CCPElem': [],
		'CCPCSS': {},
		'CCPProp': {},
	};

	this.cron = new CCPCron();
	this.cron.start();
}
var CCPG = new CCPGlobals();

// ------------------------------------

var CCPDebug = false;	// ne pas oublier de passer à false, une fois fini !!!

// ------------------------------------

// object du plugin
function CCP() {
	CCPG.self = this;

	this.version = 16.0;
	this.compatibleWith = 2.0045;
	this.saveTo = "CCPSave";
	this.options = new CCPO();
	this.saisonDrops = new CCPSD();
	this.cache = {};

	this.host = "https://ccp.vyshorin.fr/";
	this.bootstrapSrc = this.host+"ccp-bootstrap.js";
	this.versionSrc = this.host+"ccp-version.php";
	this.backup = {};

	this.dejaShimmer = [];

	this.audioCSrcDef = this.host+"gc.mp3?c="+(CCPDebug?Date.now():this.version);
	this.audioSSrcDef = this.host+"sp2.mp3?c="+(CCPDebug?Date.now():this.version);

	this.notifQueue = [];

	/*=====================================================================================
	TOOLS
	=======================================================================================*/
	this.appendToNative = function(leNative, leAppend) {
		return function() {
			var sortie = leNative.apply(null, arguments);				// fonction native à exécuter
			return leAppend.apply(null, [sortie]) || sortie;			// fonction additionnel avec en paramètre le retour de l'autre
		};
	};
	this.appendToThisNative = function(leThis, leNative, leAppend) {	// avec en plus l'attribut "this"
		return function() {
			var sortie = leNative.apply(leThis, arguments);				// fonction native à exécuter
			return leAppend.apply(null, [sortie, leThis]) || sortie;	// fonction additionnel avec en paramètre le retour de l'autre
		};
	};

	this.getBaseCps = function() {
		return Game.cookiesPs/(Game.frenzy>0?Game.frenzyPower:1);
	};
	this.getEffectiveCps = function() {
		return Game.cookiesPs*(1-Game.cpsSucked);	// Prend en compte les larves
	};
	this.baseCps = this.getBaseCps();
	this.effectiveCps = this.getEffectiveCps();

	// On considère qu'un cookie est doré tant qu'il n'apparait pas et qu'on découvre alors qu'il est wrath
	this.getGoldenMult = function() {
		var mult=1;
		for(var i in Game.shimmers) {
			var me = Game.shimmers[i];
			if(me.spawnLead && me.type=="golden" && me.life>0) {	// seulement si le cookie a spawn
				if(me.wrath>0 && Game.hasAura('Unholy Dominion')) mult*=1.1;
				else if(me.wrath==0 && Game.hasAura('Ancestral Metamorphosis')) mult*=1.1;
				// 2.0106
				if (Game.Has('Green yeast digestives')) mult*=1.01;
				if (!me.wrath) mult*=Game.eff('goldenCookieGain');
				else mult*=Game.eff('wrathCookieGain');
				break;
			}
		}
		return mult;
	};
	this.getLuckyBank = function() {
		return this.baseCps * (60*15) / 0.15 + 13;
	};
	this.getLuckyFrenzyBank = function() {
		return this.baseCps * (60*15) / 0.15 * 7 + 13;	// TODO Game.frenzyPower dragon
	};
	this.getMaxLuckyReward = function() {
		return this.getGoldenMult() * (this.baseCps * (60*15) + 13);
	};
	this.getMaxLuckyFrenzyReward = function() {
		return this.getGoldenMult() * (this.baseCps * (60*15) * 7 + 13);			// TODO Game.frenzyPower dragon
	};
	this.getLuckyReward = function() {
		// return this.getGoldenMult() * Math.min(Game.cookies*0.15 + 13, this.getMaxLuckyReward());
		return this.getGoldenMult() * Math.min(Game.cookies*0.15 + 13, this.baseCps * (60*15) + 13);
	};
	this.getLuckyFrenzyReward = function() {
		// return this.getGoldenMult() * Math.min(Game.cookies*0.15 + 13, this.getMaxLuckyFrenzyReward());
		return this.getGoldenMult() * Math.min(Game.cookies*0.15 + 13, this.baseCps * (60*15) * 7 + 13);
	};

	this.getSaisonPopupReward = function() {
		var moni=Math.max(25, Game.cookiesPs*60);	// 1 minute of cookie production, or 25 cookies - whichever is highest
		if(Game.Has('Ho ho ho-flavored frosting')) moni*=2;
		return moni;
	};

	this.howMuchPrestige = function(cookies) {
		// return Math.floor(Math.pow(cookies/1000000000000, 1/Game.HCfactor));
		return Math.floor(Game.HowMuchPrestige(cookies));
	};
	this.howMuchPrestigeInverse = function(prestige) {
		// return Math.floor(Math.pow(prestige, Game.HCfactor)*1000000000000);
		return Math.floor(Game.HowManyCookiesReset(prestige));
	};
	this.howMuchCookiesToNextPrestige = function(prestige) {
		var restant = this.howMuchPrestigeInverse(prestige) - (Game.cookiesReset + Game.cookiesEarned);
		return restant>0?restant:0;
	};

	this.wrinklersSucked = function() {
		var sucked = 0;
		for(var i in Game.wrinklers) {
			var me=Game.wrinklers[i];
			sucked += me.sucked*(me.type==1?3:1);	// shiny wrinklers rapportent 3 fois plus
		}
		var mult = 1.1;
		if(Game.Has('Sacrilegious corruption')) mult*=1.05;
		if(Game.Has('Wrinklerspawn')) mult*=1.05;
		return [sucked, sucked*mult];
	};
	this.wrinklersExist = function() {
		for(var i in Game.wrinklers) {
			if(Game.wrinklers[i].phase>0) {
				return true;
			}
		}
		return false;
	};

	this.infosSaisons = function() {
		for(var saison in CCPG.self.saisonDrops) {
			var sd = CCPG.self.saisonDrops[saison],
				arrDrops = sd.a,
				nb = 0;
			for(var i=0; i<sd.m; ++i) {
				if(Game.Has(arrDrops[i])) nb++;
			}
			sd.n = nb;
			sd.text = (sd.n==sd.m?'<span class="text-green">'+sd.n+" / "+sd.m+'</span>':sd.n+" / "+sd.m);
		}
	};

	this.getProductionTotalObject = function(obj, amount) {
		if(typeof obj === "string") obj = Game.Objects[obj];	// obli
		amount = amount || obj.amount;
		return obj.storedCps*amount*Game.globalCpsMult;
	};
	this.getProductionObject = function(obj) {
		return this.getProductionTotalObject(obj, 1);
	};
	this.getGrandmaSynergyUpgradeMultiplierDelta=function(building) {
		return (1+0.01*(1/(Game.Objects[building].id-1)));
	};

	this.getProdBonusObject = function(obj, amount) {
		amount = amount || obj.amount+1;
		var prodParSec = this.getProductionTotalObject(obj);
		var prodParSecNext = this.getProductionTotalObject(obj, amount);
		var bonus = prodParSecNext-prodParSec;

		// grandma synergies
		if((Game.Has('Farmer grandmas') && obj.name=='Farm')
		|| (Game.Has('Miner grandmas') && obj.name=='Mine')
		|| (Game.Has('Worker grandmas') && obj.name=='Factory')
		|| (Game.Has('Banker grandmas') && obj.name=='Bank')
		|| (Game.Has('Priestess grandmas') && obj.name=='Temple')
		|| (Game.Has('Witch grandmas') && obj.name=='Wizard tower')
		|| (Game.Has('Cosmic grandmas') && obj.name=='Shipment')
		|| (Game.Has('Transmuted grandmas') && obj.name=='Alchemy lab')
		|| (Game.Has('Altered grandmas') && obj.name=='Portal')
		|| (Game.Has('Grandmas\' grandmas') && obj.name=='Time machine')
		|| (Game.Has('Antigrandmas') && obj.name=='Antimatter condenser')
		|| (Game.Has('Rainbow grandmas') && obj.name=='Prism')
		|| (Game.Has('Lucky grandmas') && obj.name=='Chancemaker')) {
			bonus*=this.getGrandmaSynergyUpgradeMultiplierDelta(obj.name);
		}

		// Cursors gain X cookies for each non-cursor object owned
		if(obj.name!='Cursor') {
			var add=0;

			if (Game.Has('Thousand fingers')) add+=		0.1;
			if (Game.Has('Million fingers')) add+=		0.5;
			if (Game.Has('Billion fingers')) add+=		5;
			if (Game.Has('Trillion fingers')) add+=		50;
			if (Game.Has('Quadrillion fingers')) add+=	500;
			if (Game.Has('Quintillion fingers')) add+=	5000;
			if (Game.Has('Sextillion fingers')) add+=	50000;
			if (Game.Has('Septillion fingers')) add+=	500000;
			if (Game.Has('Octillion fingers')) add+=	5000000;

			add*=Game.Objects['Cursor'].amount;

			bonus+=add*Game.globalCpsMult;
		}

		if((Game.Has('One mind') && obj.name=='Grandma') || (Game.Has('Elder Pact') && obj.name=='Portal')) {
			var add=0,
				mult=1;

			if (Game.Has('Farmer grandmas')) mult*=2;
			if (Game.Has('Worker grandmas')) mult*=2;
			if (Game.Has('Miner grandmas')) mult*=2;
			if (Game.Has('Cosmic grandmas')) mult*=2;
			if (Game.Has('Transmuted grandmas')) mult*=2;
			if (Game.Has('Altered grandmas')) mult*=2;
			if (Game.Has('Grandmas\' grandmas')) mult*=2;
			if (Game.Has('Antigrandmas')) mult*=2;
			if (Game.Has('Rainbow grandmas')) mult*=2;
			if (Game.Has('Banker grandmas')) mult*=2;
			if (Game.Has('Priestess grandmas')) mult*=2;
			if (Game.Has('Witch grandmas')) mult*=2;
			if (Game.Has('Lucky grandmas')) mult*=2;
			if (Game.Has('Bingo center/Research facility')) mult*=4;
			if (Game.Has('Ritual rolling pins')) mult*=2;
			if (Game.Has('Naughty list')) mult*=2;
			// 2.0106
			if (Game.Has('Elderwort biscuits')) mult*=1.02;
			// mult*=Game.eff('grandmaCps');
			// mult*=Game.GetTieredCpsMult(me);

			if (Game.Has('Steel-plated rolling pins')) mult*=2;
			if (Game.Has('Lubricated dentures')) mult*=2;
			if (Game.Has('Prune juice')) mult*=2;
			if (Game.Has('Double-thick glasses')) mult*=2;
			if (Game.Has('Aging agents')) mult*=2;


			if(obj.name=='Grandma') {
				// Each grandma gains +1 base CpS for every 50 grandmas
				// if(Game.Has('One mind')) add+=(Game.Objects['Grandma'].amount+1)*0.02;
				if(Game.Has('One mind')) add+=(amount)*0.02;
				// Each grandma gains another +1 base CpS for every 50 grandmas
				// if(Game.Has('Communal brainsweep')) add+=(Game.Objects['Grandma'].amount+1)*0.02;
				if(Game.Has('Communal brainsweep')) add+=(amount)*0.02;
			} else {
				// Each grandma gains +1 base CpS for every 20 portals
				// add+=Game.Objects['Grandma'].amount*0.05;
				add+=Game.Objects['Grandma'].amount*0.05*(obj.amount-amount);
			}

			bonus+=add*mult*Game.globalCpsMult;
		}

		// synergies
		for(var i in obj.synergies) {
			var syn=obj.synergies[i];
			if(Game.Has(syn.name)) {
				if(syn.buildingTie1.name==obj.name) bonus+=0.001*syn.buildingTie2.amount;
				else if(syn.buildingTie2.name==obj.name) bonus+=0.05*syn.buildingTie1.amount;
			}
		}

		return bonus;
	};

	this.getProdBonusUpgrade = function(upg) {
		var add=0,
			mult=0;

		if(upg.type=='cookie' || upg.pool=='cookie') {	// 1.05
			var power = (typeof upg.power === "function"?upg.power(upg):upg.power);
			return this.effectiveCps*power*0.01/Game.globalCpsMult*this.getMultMilk();
			// frenzy ?
			// grandmapocalypse ?
		} else {
			switch(upg.name) {
				case "Reinforced index finger" :
					return this.getProductionObject('Cursor')*Game.Objects['Cursor'].amount;
				case "Carpal tunnel prevention cream" :
				case "Ambidextrous" :
					return this.getProductionObject('Cursor')*Game.Objects['Cursor'].amount;
				case "Thousand fingers" :
					return this.getBonusCursor(0.1);
				case "Million fingers" :
					return this.getBonusCursor(0.5);
				case "Billion fingers" :
					return this.getBonusCursor(5);
				case "Trillion fingers" :
					return this.getBonusCursor(50);
				case "Quadrillion fingers" :
					return this.getBonusCursor(500);
				case "Quintillion fingers" :
					return this.getBonusCursor(5000);
				case "Sextillion fingers" :
					return this.getBonusCursor(50000);
				case "Septillion fingers" :
					return this.getBonusCursor(500000);
				case "Octillion fingers" :
					return this.getBonusCursor(5000000);

				// TODO bonus de clique
				/* case "Plastic mouse" :
				break;
				case "Iron mouse" :
				break;
				case "Titanium mouse" :
				break;
				case "Adamantium mouse" :
				break;
				case "Unobtainium mouse" :
				break;
				case "Eludium mouse" :
				break;
				case "Wishalloy mouse" :
				break; */

				case "Timeproof hair dyes" :
				case "Good manners" :
				case "Forwards from grandma" :
				case "Steel-plated rolling pins" :
				case "Lubricated dentures" :
				case "Prune juice" :
				case "Double-thick glasses" :
				case "Aging agents" :
				case "Xtreme walkers" :
				case "The Unbridling" :
				case "Reverse dementia" :
					return this.getProductionObject('Grandma')*Game.Objects['Grandma'].amount;

				case "Farmer grandmas" :
				case "Worker grandmas" :
				case "Miner grandmas" :
				case "Cosmic grandmas" :
				case "Transmuted grandmas" :
				case "Altered grandmas" :
				case "Grandmas' grandmas" :
				case "Antigrandmas" :
				case "Rainbow grandmas" :
				case "Banker grandmas" :
				case "Priestess grandmas" :
				case "Witch grandmas" :
				case "Lucky grandmas" :
					// TODO calculer synergie batiments
					return this.getProductionObject('Grandma')*Game.Objects['Grandma'].amount;

				case "Barnstars" :
				case "Lindworms" :
				case "Cheap hoes" :
				case "Fertilizer" :
				case "Cookie trees" :
				case "Genetically-modified cookies" :
				case "Gingerbread scarecrows" :
				case "Pulsar sprinklers" :
				case "Fudge fungus" :
				case "Wheat triffids" :
				case "Humane pesticides" :
					return this.getProductionObject('Farm')*Game.Objects['Farm'].amount;

				case "Mine canaries" :
				case "Bore again" :
				case "Sugar gas" :
				case "Megadrill" :
				case "Ultradrill" :
				case "Ultimadrill" :
				case "H-bomb mining" :
				case "Coreforge" :
				case "Planetsplitters" :
				case "Canola oil wells" :
				case "Mole people" :
					return this.getProductionObject('Mine')*Game.Objects['Mine'].amount;

				case "Brownie point system" :
				case '"Volunteer" interns' :
				case "Sturdier conveyor belts" :
				case "Child labor" :
				case "Sweatshop" :
				case "Radium reactors" :
				case "Recombobulators" :
				case "Deep-bake process" :
				case "Cyborg workforce" :
				case "78-hour days" :
				case "Machine learning" :
					return this.getProductionObject('Factory')*Game.Objects['Factory'].amount;

				case "Grand supercycles" :
				case "Rules of acquisition" :
				case "Taller tellers" :
				case "Scissor-resistant credit cards" :
				case "Acid-proof vaults" :
				case "Chocolate coins" :
				case "Exponential interest rates" :
				case "Financial zen" :
				case "Way of the wallet" :
				case "The stuff rationale" :
				case "Edible money" :
					return this.getProductionObject('Bank')*Game.Objects['Bank'].amount;

				case "Psalm-reading" :
				case "War of the gods" :
				case "Golden idols" :
				case "Sacrifices" :
				case "Delicious blessing" :
				case "Sun festival" :
				case "Enlarged pantheon" :
				case "Great Baker in the sky" :
				case "Creation myth" :
				case "Theocracy" :
				case "Sick rap prayers" :
					return this.getProductionObject('Temple')*Game.Objects['Temple'].amount;

				case "Immobile spellcasting" :
				case "Electricity" :
				case "Pointier hats" :
				case "Beardlier beards" :
				case "Ancient grimoires" :
				case "Kitchen curses" :
				case "School of sorcery" :
				case "Dark formulas" :
				case "Cookiemancy" :
				case "Rabbit trick" :
				case "Deluxe tailored wands" :
					return this.getProductionObject('Wizard tower')*Game.Objects['Wizard tower'].amount;

				case "Restaurants at the end of the universe" :
				case "Universal alphabet" :
				case "Vanilla nebulae" :
				case "Wormholes" :
				case "Frequent flyer" :
				case "Warp drive" :
				case "Chocolate monoliths" :
				case "Generation ship" :
				case "Dyson sphere" :
				case "The final frontier" :
				case "Autopilot" :
					return this.getProductionObject('Shipment')*Game.Objects['Shipment'].amount;

				case "On second thought" :
				case "Public betterment" :
				case "Antimony" :
				case "Essence of dough" :
				case "True chocolate" :
				case "Ambrosia" :
				case "Aqua crustulae" :
				case "Origin crucible" :
				case "Theory of atomic fluidity" :
				case "Beige goo" :
				case "The advent of chemistry" :
					return this.getProductionObject('Alchemy lab')*Game.Objects['Alchemy lab'].amount;

				case "Dimensional garbage gulper" :
				case "Embedded microportals" :
				case "Ancient tablet" :
				case "Insane oatling workers" :
				case "Soul bond" :
				case "Sanity dance" :
				case "Brane transplant" :
				case "Deity-sized portals" :
				case "End of times back-up plan" :
				case "Maddening chants" :
				case "The real world" :
					return this.getProductionObject('Portal')*Game.Objects['Portal'].amount;

				case "Additional clock hands" :
				case "Nostalgia" :
				case "Flux capacitors" :
				case "Time paradox resolver" :
				case "Quantum conundrum" :
				case "Causality enforcer" :
				case "Yestermorrow comparators" :
				case "Far future enactment" :
				case "Great loop hypothesis" :
				case "Cookietopian moments of maybe" :
				case "Second seconds" :
					return this.getProductionObject('Time machine')*Game.Objects['Time machine'].amount;

				case "Baking Nobel prize" :
				case "The definite molecule" :
				case "Sugar bosons" :
				case "String theory" :
				case "Large macaron collider" :
				case "Big bang bake" :
				case "Reverse cyclotrons" :
				case "Nanocosmics" :
				case "The Pulse" :
				case "Some other super-tiny fundamental particle? Probably?" :
				case "Quantum comb" :
					return this.getProductionObject('Antimatter condenser')*Game.Objects['Antimatter condenser'].amount;

				case "Reverse theory of light" :
				case "Light capture measures" :
				case "Gem polish" :
				case "9th color" :
				case "Chocolate light" :
				case "Grainbow" :
				case "Pure cosmic light" :
				case "Glow-in-the-dark" :
				case "Lux sanctorum" :
				case "Reverse shadows" :
				case "Crystal mirrors" :
					return this.getProductionObject('Prism')*Game.Objects['Prism'].amount;

				case "Revised probabilistics" :
				case "0-sided dice" :
				case "Your lucky cookie" :
				case "\"All Bets Are Off\"" :
				case "Winning lottery ticket" :
				case "Four-leaf clover field" :
				case "A recipe book about books" :
				case "Leprechaun village" :
				case "Improbability drive" :
				case "Antisuperstistronics" :
				case "Bunnypedes" :
					return this.getProductionObject('Chancemaker')*Game.Objects['Chancemaker'].amount;

				/* case "Lucky day" :
				break;
				case "Serendipity" :
				break;
				case "Get lucky" :
				break; */

				case "Bingo center/Research facility" :
					return 3*this.getProductionObject('Grandma')*Game.Objects['Grandma'].amount;
				/* case "Persistent memory" :
				break; */
				case "Specialized chocolate chips" :
					mult=0.01;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "Designer cocoa beans" :
					mult=0.02;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "Ritual rolling pins" :
					return this.getProductionObject('Grandma')*Game.Objects['Grandma'].amount;
				case "Underworld ovens" :
					mult=0.03;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "One mind" :
				case "Communal brainsweep" :
					add = Game.Objects['Grandma'].amount*0.02;

					// if (Game.Has('Farmer grandmas')) mult++;
					// if (Game.Has('Worker grandmas')) mult++;
					// if (Game.Has('Miner grandmas')) mult++;
					// if (Game.Has('Cosmic grandmas')) mult++;
					// if (Game.Has('Transmuted grandmas')) mult++;
					// if (Game.Has('Altered grandmas')) mult++;
					// if (Game.Has('Grandmas\' grandmas')) mult++;
					// if (Game.Has('Antigrandmas')) mult++;
					// if (Game.Has('Rainbow grandmas')) mult++;
					// if (Game.Has('Banker grandmas')) mult++;
					// if (Game.Has('Priestess grandmas')) mult++;
					// if (Game.Has('Witch grandmas')) mult++;
					// if (Game.Has('Bingo center/Research facility')) mult+=2;
					// if (Game.Has('Ritual rolling pins')) mult++;
					// if (Game.Has('Naughty list')) mult++;

					// mult+=Game.Has('Steel-plated rolling pins')+Game.Has('Lubricated dentures')+Game.Has('Prune juice')+Game.Has('Double-thick glasses')+Game.Has('Aging agents');

					// return add*Math.pow(2,mult)*Game.Objects['Grandma'].amount*Game.globalCpsMult;
					return add*Game.Objects['Grandma'].amount*Game.globalCpsMult;
				case "Exotic nuts" :
					mult=0.04;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "Arcane sugar" :
					mult=0.05;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "Elder Pact" :
					add = Game.Objects['Portal'].amount*0.05;

					// if (Game.Has('Farmer grandmas')) mult++;
					// if (Game.Has('Worker grandmas')) mult++;
					// if (Game.Has('Miner grandmas')) mult++;
					// if (Game.Has('Cosmic grandmas')) mult++;
					// if (Game.Has('Transmuted grandmas')) mult++;
					// if (Game.Has('Altered grandmas')) mult++;
					// if (Game.Has('Grandmas\' grandmas')) mult++;
					// if (Game.Has('Antigrandmas')) mult++;
					// if (Game.Has('Rainbow grandmas')) mult++;
					// if (Game.Has('Banker grandmas')) mult++;
					// if (Game.Has('Priestess grandmas')) mult++;
					// if (Game.Has('Witch grandmas')) mult++;
					// if (Game.Has('Bingo center/Research facility')) mult+=2;
					// if (Game.Has('Ritual rolling pins')) mult++;
					// if (Game.Has('Naughty list')) mult++;

					// mult+=Game.Has('Steel-plated rolling pins')+Game.Has('Lubricated dentures')+Game.Has('Prune juice')+Game.Has('Double-thick glasses')+Game.Has('Aging agents');

					// return add*Math.pow(2,mult)*Game.Objects['Grandma'].amount*Game.globalCpsMult;
					return add*Game.Objects['Grandma'].amount*Game.globalCpsMult;
				case "Elder Covenant" :
					return Game.cookiesPs*0.05*-1;
				case "Revoke Elder Covenant" :
					return Game.cookiesPs*0.05/0.95;
				// case "Sacrificial rolling pins" :
				// break;

				case "Kitten helpers" :
					return this.getBonusMilk(0.1);
				case "Kitten workers" :
				case "Kitten analysts" :
					return this.getBonusMilk(0.125);
				case "Kitten engineers" :
				case "Kitten marketeers" :
					return this.getBonusMilk(0.15);
				case "Kitten overseers" :
				case "Kitten assistants to the regional manager" :
					return this.getBonusMilk(0.175);
				case "Kitten managers" :
				case "Kitten accountants" :
				case "Kitten specialists" :
				case "Kitten experts" :
				case "Kitten consultants" :
					return this.getBonusMilk(0.2);
				case "Kitten angels" :
					return this.getBonusMilk(0.1);

				case "Chicken egg" :
				case "Duck egg" :
				case "Turkey egg" :
				case "Quail egg" :
				case "Robin egg" :
				case "Ostrich egg" :
				case "Cassowary egg" :
				case "Salmon roe" :
				case "Frogspawn" :
				case "Shark egg" :
				case "Turtle egg" :
				case "Ant larva" :
					mult=0.01;
					return this.effectiveCps*mult;

				/* case "Golden goose egg" :
				break;
				case "Faberge egg" :
				break;
				case "Wrinklerspawn" :
				break;
				case "Cookie egg" :
				break;
				case "Omelette" :
				break;
				case "Chocolate egg" :
				break; */
				case "Century egg" :
					// the boost increases a little every day, with diminishing returns up to +10% on the 100th day
					var day=Math.floor((Date.now()-Game.startDate)/1000/10)*10/60/60/24;
					day=Math.min(day,100);
					mult=(1-Math.pow(1-day/100,3))*10;

					return this.effectiveCps*0.01*mult;
				case "\"egg\"" :
				case "\"god\"" :
					return 9*Game.globalCpsMult;

				// case "A festive hat" :
				// break;
				case "Increased merriness" :
					mult=0.15;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "Improved jolliness" :
					mult=0.15;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "A lump of coal" :
					mult=0.01;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();
				case "An itchy sweater" :
					mult=0.01;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();

				/* case "Reindeer baking grounds" :
				break;
				case "Weighted sleighs" :
				break;
				case "Ho ho ho-flavored frosting" :
				break; */

				/* case "Season savings" :
				break;
				case "Toy workshop" :
				break; */
				case "Naughty list" :
					return this.getProductionObject('Grandma')*Game.Objects['Grandma'].amount;
				/* case "Santa's bottomless bag" :
				break;
				case "Santa's helpers" :
				break; */
				case "Santa's legacy" :
					mult=0.03;
					return this.effectiveCps*(Game.santaLevel+1)*mult/Game.globalCpsMult*this.getMultMilk();
				case "Santa's milk and cookies" :
					var milkMult=0.05;
					if (Game.hasAura('Breath of Milk')) milkMult*=1.05;
					if (Game.Has("Kitten helpers")) mult+=0.1;
					if (Game.Has("Kitten workers")) mult+=0.125;
					if (Game.Has("Kitten engineers")) mult+=0.15;
					if (Game.Has("Kitten overseers")) mult+=0.175;
					if (Game.Has("Kitten managers")) mult+=0.2;
					if (Game.Has("Kitten accountants")) mult+=0.2;
					if (Game.Has("Kitten specialists")) mult+=0.2;
					if (Game.Has("Kitten experts")) mult+=0.2;
					if (Game.Has("Kitten consultants")) mult+=0.2;
					if (Game.Has("Kitten assistants to the regional manager")) mult+=0.175;
					if (Game.Has("Kitten marketeers")) mult+=0.15;
					if (Game.Has("Kitten analysts")) mult+=0.125;
					if (Game.Has("Kitten angels")) mult+=0.1;

					return mult*milkMult*Game.milkProgress*this.effectiveCps/this.getMultMilk();
				case "Santa's dominion" :
					mult=0.2;
					return this.effectiveCps*mult/Game.globalCpsMult*this.getMultMilk();

				case "Golden switch [off]" :
					mult=0.5;
					return this.baseCps*mult;
				case "Golden switch [on]" :
					mult=-1/3;
					return this.baseCps*mult;

				// synergies
				case "Future almanacs" :
					return this.getBonusSynergie('Farm', 'Time machine');
				case "Rain prayer" :
					return this.getBonusSynergie('Farm', 'Temple');
				case "Seismic magic" :
					return this.getBonusSynergie('Mine', 'Wizard tower');
				case "Asteroid mining" :
					return this.getBonusSynergie('Mine', 'Shipment');
				case "Quantum electronics" :
					return this.getBonusSynergie('Factory', 'Antimatter condenser');
				case "Temporal overclocking" :
					return this.getBonusSynergie('Factory', 'Time machine');
				case "Contracts from beyond" :
					return this.getBonusSynergie('Bank', 'Portal');
				case "Printing presses" :
					return this.getBonusSynergie('Bank', 'Factory');
				case "Paganism" :
					return this.getBonusSynergie('Temple', 'Portal');
				case "God particle" :
					return this.getBonusSynergie('Temple', 'Antimatter condenser');
				case "Arcane knowledge" :
					return this.getBonusSynergie('Wizard tower', 'Alchemy lab');
				case "Magical botany" :
					return this.getBonusSynergie('Wizard tower', 'Farm');
				case "Fossil fuels" :
					return this.getBonusSynergie('Shipment', 'Mine');
				case "Shipyards" :
					return this.getBonusSynergie('Shipment', 'Factory');
				case "Primordial ores" :
					return this.getBonusSynergie('Alchemy lab', 'Mine');
				case "Gold fund" :
					return this.getBonusSynergie('Alchemy lab', 'Bank');
				case "Infernal crops" :
					return this.getBonusSynergie('Portal', 'Farm');
				case "Abysmal glimmer" :
					return this.getBonusSynergie('Portal', 'Prism');
				case "Relativistic parsec-skipping" :
					return this.getBonusSynergie('Time machine', 'Shipment');
				case "Primeval glow" :
					return this.getBonusSynergie('Time machine', 'Prism');
				case "Extra physics funding" :
					return this.getBonusSynergie('Antimatter condenser', 'Bank');
				case "Chemical proficiency" :
					return this.getBonusSynergie('Antimatter condenser', 'Alchemy lab');
				case "Light magic" :
					return this.getBonusSynergie('Prism', 'Wizard tower');
				case "Mystical energies" :
					return this.getBonusSynergie('Prism', 'Temple');

				case "Heavenly chip secret" :
					return this.getBonusPrestige(0.05);
				case "Heavenly cookie stand" :
					return this.getBonusPrestige(0.20);
				case "Heavenly bakery" :
				case "Heavenly confectionery" :
				case "Heavenly key" :
					return this.getBonusPrestige(0.25);
			}
		}
		return 0;
	};

	this.getBonusCursor=function(add) {
		var num=0;
		for(var i in Game.Objects) {if(Game.Objects[i].name!='Cursor') num+=Game.Objects[i].amount;}
		return add*num*Game.Objects['Cursor'].amount*Game.globalCpsMult;
	};
	this.getBonusMilk = function(milkMult) {
		var mult=Game.Has('Santa\'s milk and cookies')?1.05:1;
		mult*=Game.hasAura('Breath of Milk')?1.05:1;
		return mult*this.effectiveCps*Game.milkProgress*milkMult;
	};
	this.getBonusPrestige = function(heavenlyMult) {
		var mult = Game.hasAura('Dragon God')?1.05:1;
		var bonus = (+Game.prestige)*0.01*Game.heavenlyPower*heavenlyMult*mult;
		return bonus*this.effectiveCps/Game.globalCpsMult*this.getMultMilk();
	};
	this.getBonusSynergie = function(bat1, bat2) {
		return (0.05*this.getProductionTotalObject(bat1)*Game.Objects[bat2].amount
			+ 0.001*this.getProductionTotalObject(bat2)*Game.Objects[bat1].amount);
	};

	this.getMultMilk = function() {
		var mult = 1,
			milkMult=1;
		if (Game.Has('Santa\'s milk and cookies')) milkMult*=1.05;
		if (Game.hasAura('Breath of Milk')) milkMult*=1.05;
		if (Game.Has('Kitten helpers')) mult*=(1+Game.milkProgress*0.1*milkMult);
		if (Game.Has('Kitten workers')) mult*=(1+Game.milkProgress*0.125*milkMult);
		if (Game.Has('Kitten engineers')) mult*=(1+Game.milkProgress*0.15*milkMult);
		if (Game.Has('Kitten overseers')) mult*=(1+Game.milkProgress*0.175*milkMult);
		if (Game.Has('Kitten managers')) mult*=(1+Game.milkProgress*0.2*milkMult);
		if (Game.Has('Kitten accountants')) mult*=(1+Game.milkProgress*0.2*milkMult);
		if (Game.Has('Kitten specialists')) mult*=(1+Game.milkProgress*0.2*milkMult);
		if (Game.Has('Kitten experts')) mult*=(1+Game.milkProgress*0.2*milkMult);
		if (Game.Has('Kitten consultants')) mult*=(1+Game.milkProgress*0.2*milkMult);
		if (Game.Has('Kitten assistants to the regional manager')) mult*=(1+Game.milkProgress*0.175*milkMult);
		if (Game.Has('Kitten marketeers')) mult*=(1+Game.milkProgress*0.15*milkMult);
		if (Game.Has('Kitten analysts')) mult*=(1+Game.milkProgress*0.125*milkMult);
		if (Game.Has('Kitten angels')) mult*=(1+Game.milkProgress*0.1*milkMult);
		return mult;
	};

	this.formatTime = function(t, compressed, noZero) {		// de CM
		var time	= t | 0,	// OU binaire qui force en int, bien plus rapide
			days	= (time/86400 | 0) % 999,
			hours	= (time/3600 | 0) % 24,
			minutes	= (time/60 | 0) % 60,
			seconds	= time % 60,
			units	= [' jours, ', ' heures, ', ' minutes, ', ' secondes'],
			formatted = "";

		if(!isFinite(time)) {
			return '<span class="text-red">Jamais<span>';
		} else if(time/86400 > 1000 || time<0) {	// <0 en overflow
			return '<span class="text-red">&gt; 1 000 jours</span>';
		}

		if(!compressed) {
			if(days<=1)    units[0] = ' jour, ';
			if(hours<=1)   units[1] = ' heure, ';
			if(minutes<=1) units[2] = ' minute, ';
			if(seconds<=1) units[3] = ' seconde';
		} else {
			units = ['j, ', 'h, ', 'm, ', 's'];
		}

		if(!noZero) {
			if(days>0) {
				formatted += days + units[0];
			}
			if(days>0 || hours>0) {
				formatted += hours + units[1];
			}
			if(days>0 || hours>0 || minutes>0) {
				formatted += minutes + units[2];
			}
			if(days>0 || hours>0 || minutes>0 || seconds>0) {
				formatted += seconds + units[3];
			}
		} else {
			if(days>0)    formatted += days + units[0];
			if(hours>0)   formatted += hours + units[1];
			if(minutes>0) formatted += minutes + units[2];
			if(seconds>0) formatted += seconds + units[3];
			if(formatted.substr(-2)==', ') formatted = formatted.slice(0, -2);	// enlève le ', ' final
		}
		return formatted?formatted:'<span class="text-green">Fini !</span>';
	};

	/*=====================================================================================
	GRAPHIQUES
	=======================================================================================*/
	this.creerPanel = function() {
		// Panneau d'administration du plugin
		if(!CCPG.CCPPanel) {
			var panel = CCPT.create("div");
			CCPT.attr(panel, "id", "CCPPanel");
			l("centerArea").appendChild(panel);

			CCPG.CCPPanel = l("CCPPanel");
		}

		var str = ''
			+ '<div id="CCPPanelX" class="close menuClose">x</div>'

			+ '<div class="section">Statistiques</div>'

			+ '<div class="subsection">'
			+ '<div class="title"><span class="icon iconCookieDore"></span>Cookies Dorés</div>'
			+ '<div class="listing"><b>Nombre d\'or Lucky :</b> <span id="CCPStatNombreDorL"></span></div>'
			+ '<div class="listing"><b>Nombre d\'or Lucky + Frenzy :</b> <span id="CCPStatNombreDorLF"></span></div>'
			+ '<div class="listing"><b>Bonus Lucky :</b> <span id="CCPStatRecompenseL"></span></div>'
			+ '<div class="listing"><b>Bonus Lucky + Frenzy :</b> <span id="CCPStatRecompenseLF"></span></div>'

			+ '<div id="CCPStatNextCDDiv">'
			+ '<div class="listing"><b>Prochain cookie doré : <span id="CCPStatNextCD"><span id="CCPStatNextCDBarre"></span><span id="CCPStatNextCDFond"></span></span></b></div>'
			+ '<div class="listing"><b>Temps max restant :</b> <span id="CCPStatNextCDTemps"></span></div>'
			+ '</div>'

			+ '<div class="title"><span class="icon iconRenne"></span>Rennes</div>'
			+ '<div class="listing"><b>Bonus Renne :</b> <span id="CCPStatRecompenseS"></span></div>'

			+ '<div id="CCPStatNextSPDiv">'
			+ '<div class="listing"><b>Prochain renne : <span id="CCPStatNextSP"><span id="CCPStatNextSPBarre"></span><span id="CCPStatNextSPFond"></span></span></b></div>'
			+ '<div class="listing"><b>Temps max restant :</b> <span id="CCPStatNextSPTemps"></span></div>'
			+ '</div>'

			+ '<div class="title"><span class="icon iconPrestige"></span>Prestige</div>'
			+ '<div class="listing"><b>Prestige maximum :</b> <span id="CCPStatPrestige"></span></div>'
			+ '<div class="listing"><b>Cookies pour prochain Legacy :</b> <span id="CCPStatPrestigeNextCookies"></span></div>'
			+ '<div class="listing"><b>Temps pour prochain Legacy :</b> <span id="CCPStatPrestigeNextTime"></span></div>'
			+ '<div class="listing"><b>Cookies pour <input id="CCPXPrestige" type="number" min="0"/> de Prestige :</b> <span id="CCPStatPrestigeXCookies"></span></div>'

			+ '<div class="title"><span class="icon iconWrinkler"></span>Wriklers</div>'
			+ '<div class="listing"><b>Cookies sucés par les larves :</b> <span id="CCPStatSucked"></span></div>'
			+ '<div class="listing"><b>Récompense en tuant les larves :</b> <span id="CCPStatSuckedR"></span></div>'

			+ '<div class="title"><span class="icon iconSaisons"></span>Saisons</div>'
			+ '<div class="listing"><b>Upgrades d\'halloween débloqués :</b> <span id="CCPStatHalloween"></span></div>'
			+ '<div class="listing"><b>Upgrades de noël débloqués :</b> <span id="CCPStatNoel"></span></div>'
			+ '<div class="listing"><b>Upgrades de la st Valentin débloqués :</b> <span id="CCPStatValentin"></span></div>'
			+ '<div class="listing"><b>Upgrades d\'œufs de Pâques débloqués :</b> <span id="CCPStatEgg"></span></div>'
			+ '<div class="listing"><b>Upgrades d\'œufs de Pâques rares débloqués :</b> <span id="CCPStatRareEgg"></span></div>'
			+ '<div class="listing"><b>Upgrades du jardin débloqués :</b> <span id="CCPStatGarden"></span></div>'

			+ '<div class="title"><span class="icon iconBâtiments"></span>Bâtiments</div>'
			+ '<div class="listing"><b>Production proportionnelle de chaque bâtiment :</b> </br><span id="CCPProdBat"></span></div>'

			+ '</div>'

			+ '<div class="section">Options</div>'

			+ '<div class="subsection">'
			+ '<div class="title"><span class="icon iconGénéral"></span>Général</div>'
			// + '<div class="title"><span class="icon iconClef"></span>Général</div>'
			+ '<div class="listing"><a class="option" '+Game.clickStr+'="CCPG.self.save(true);PlaySound(\'snd/tick.mp3\')">Sauvegarder Options CCP</a><label>(Sauvegarde les réglages de CCP)</label></div>'
			+ '<div class="listing"><a class="option" '+Game.clickStr+'="CCPG.self.load(true);PlaySound(\'snd/tick.mp3\')">Charger Options CCP</a><label>(Recharge la sauvegarde des réglages de CCP)</label></div>'
			+ (CCPDebug?'<div class="listing"><a class="option warning" '+Game.clickStr+'="CCPG.self.reset(true);PlaySound(\'snd/tick.mp3\')">Reset Options CCP</a><label>(Efface la sauvegarde)</label></div>':'')
			+ '<div class="listing"><a class="option warning" '+Game.clickStr+'="CCPG.self.recharger();PlaySound(\'snd/tick.mp3\')">Recharger CCP</a><label>(relance complètement CCP)</label><span class="warning">Attention : abuser de ce bouton peut faire baisser les performances</span></div>'
			+ '<div class="listing"><a class="option warning" '+Game.clickStr+'="CCPG.self.kill();PlaySound(\'snd/tick.mp3\')">Quitter CCP</a><label>(supprime CCP de Cookie Clicker)</label></div>'
			+ (CCPDebug?'<div class="listing"><a class="option" '+Game.clickStr+'="Game.OpenSesame();PlaySound(\'snd/tick.mp3\')">Sésame, ouvre-toi</a><label>(debugger natif)</label></div>':'')
			// + '</div>'
			;

		var titlePrev = "";

		for(var nom in this.options) {
			var opt = this.options[nom];
			var title = opt.menu;
			if(title!=titlePrev) {
				titlePrev = title;
				str += '<div class="title"><span class="icon icon'+title+'"></span>'+title+'</div>';
			}
			str += '<div class="listing">';
			switch(opt.type) {
				case "button" :
					var label = (opt.params.label!=''?'<label>'+opt.params.label+'</label>':'');
					str += Game.WriteButton(opt.name,opt.name,opt.params.on,opt.params.off,'CCPG.self.options.'+opt.name+'.update();')+label;
				break;
				case "range" :
					str += '<b>'+opt.params.label+' : </b>';
					str += '<div class="sliderBox">';
					str += '<div style="float:right;" id="'+opt.name+'RightText"><span id="txt'+opt.name+'">'+CCPG.self.formatTime(opt.value/1000, false, true)+'</span></div>';
					str += '<input class="slider" style="clear:both;" type="range" min="'+opt.params.min+'" max="'+opt.params.max+'" step="'+opt.params.pas+'"';
					str += ' value="'+opt.value+'" onchange="CCPG.self.options.'+opt.name+'.update(+this.value);" oninput="CCPG.self.options.'+opt.name+'.update(+this.value);" onmouseup="PlaySound(\'snd/tick.mp3\');" id="'+opt.name+'"/></div>';
				break;
				case "inputNum" :
					str += '<b>'+opt.params.label+' : <input id="'+opt.name+'" value="'+opt.value+'" onchange="CCPG.self.options.'+opt.name+'.update(+this.value);" type="number" min="0"/></b>';
				break;
				case "inputStr" :
					str += '<b>'+opt.params.label+' : <input id="'+opt.name+'" value="'+opt.value+'" onchange="CCPG.self.options.'+opt.name+'.update(this.value);"/></b>';
				break;
				case "inputAud" :
					str += '<b>'+opt.params.label+' : <input id="'+opt.name+'" value="'+opt.value+'" onchange="CCPG.self.options.'+opt.name+'.update(this.value);"/></b>';
					str += '<a '+Game.clickStr+'="'+opt.params.ajouer+'" class="option" style="margin-left:10px;">Jouer le son</a>';
				break;
				case "buttonSub" :
					var label = (opt.params.label!=''?'<label>'+opt.params.label+'</label>':'');
					str += '<a id="'+opt.name+'" '+Game.clickStr+'="CCPG.self.options.'+opt.name+'.update();" class="option">'+opt.params.text+'</a>'+label;
				break;
				case "buttonLink" :
					var label = (opt.params.label!=''?'<label>'+opt.params.label+'</label>':'');
					str += '<a id="'+opt.name+'" '+Game.clickStr+'="CCPG.self.options.'+opt.name+'.update();" class="option" href="'+opt.href+'">'+opt.params.text+'</a>'+label;
				break;
				case "inputPwd" :
					str += '<b>'+opt.params.label+' : <input id="'+opt.name+'" value="'+opt.value+'" onchange="CCPG.self.options.'+opt.name+'.update(this.value);" type="password"/></b>';
					str += '<span '+Game.clickStr+'="CCPG.self.switchPwd(\''+opt.name+'\');" class="oeil">&#128065;</span>';
				break;
			}
			str += '</div>';
		}

		str += '</div>';

		try {
			if(typeof _creerPanel === "function") {
				var retAddon = _creerPanel();
				if(retAddon) {
					str += '<div class="section">Addon</div>'

						+ '<div class="subsection">'
						// voici un exemple de titre et de ligne à retourner
						// + '<div class="title"><span class="icon iconCookieDore"></span>Cookies Dorés</div>'
						// + '<div class="listing"><b>Nombre d\'or Lucky :</b> <span id="CCPStatNombreDorL"></span></div>'
						+ retAddon
						+ '</div>';
				}
			}
		} catch(ex) {warn("Erreur Addon CCP : "+ex);}

		if(!CCPDebug) {
			str += this.credits();
		}

		// Menu afin de pouvoir acheter/vendre des upgrades pour débugger
		if(CCPDebug) {
			str += ''
				+ '<div class="section">Debug</div>'

				+ '<div class="listing warning" style="text-align:justify;">'
				+ 'ATTENTION : cette partie du plugin doit être utilisé avec précaution et parcimonie '
				+ 'car elle sert uniquement au débuggage '
				+ 'et son utilisation en dehors de cet usage serait de la triche '
				+ 'et peut provoquer un fonctionnement étrange du jeu.'
				+ '</div>'

				+ '<div class="subsection">'
				+ '<div class="title">Upgrades</div>'
				;

			// '', cookie, toggle, debug, prestige, prestigeDecor
			var sortUpgById = Game.UpgradesById.concat([]);
			sortUpgById.sort(function(a, b) {	// https://fr.wikipedia.org/wiki/Algorithme_de_tri#Caract.C3.A8re_stable
				return a.order-b.order;
			});

			for(var nom in sortUpgById) {
				var upg = sortUpgById[nom];
				if(upg.vanilla && upg.pool!='debug') {

					var tooltip = '<div style="min-width:200px;">'
						// + '<div style="float:right;"><span class="price">'+Beautify(Math.round(upg.getPrice()))+'</span></div>'
						// + '<small>[Upgrade]</small>'
						+ '<div class="name">'+upg.name+'</div>'
						+ '<div class="description">'+upg.desc+'</div>'
						+ '</div>';

					str += ''
						+ '<div class="listing">'
						+ '<span class="icon" '+Game.getTooltip(tooltip)+' style="background-position: calc('+upg.icon[0]+' * -48px) calc('+upg.icon[1]+' * -48px);"></span>'
						+ '<input id="upg'+upg.id+'" type="checkbox"'+(upg.bought?' checked':'')+' onchange="Game.UpgradesById['+upg.id+'].toggle();"/>'
						+ '<label for="upg'+upg.id+'">'+upg.name+'</label>'
						+ '</div>';
				}
			}

			str += '</div>';
		}

		str += '<div style="padding-bottom:128px;"></div>';
		CCPG.CCPPanel.innerHTML = str;

		this.cache.CCPStatNombreDorL = l("CCPStatNombreDorL");
		this.cache.CCPStatNombreDorLF = l("CCPStatNombreDorLF");
		this.cache.CCPStatRecompenseL = l("CCPStatRecompenseL");
		this.cache.CCPStatRecompenseLF = l("CCPStatRecompenseLF");

		this.cache.CCPStatNextCDDiv = l("CCPStatNextCDDiv");
		this.cache.CCPStatNextCDBarre = l("CCPStatNextCDBarre");
		this.cache.CCPStatNextCDFond = l("CCPStatNextCDFond");
		this.cache.CCPStatNextCDTemps = l("CCPStatNextCDTemps");

		this.cache.CCPStatRecompenseS = l("CCPStatRecompenseS");

		this.cache.CCPStatNextSPDiv = l("CCPStatNextSPDiv");
		this.cache.CCPStatNextSPBarre = l("CCPStatNextSPBarre");
		this.cache.CCPStatNextSPFond = l("CCPStatNextSPFond");
		this.cache.CCPStatNextSPTemps = l("CCPStatNextSPTemps");

		this.cache.CCPStatPrestige = l("CCPStatPrestige");
		this.cache.CCPStatPrestigeNextCookies = l("CCPStatPrestigeNextCookies");
		this.cache.CCPStatPrestigeNextTime = l("CCPStatPrestigeNextTime");
		this.cache.CCPXPrestige = l("CCPXPrestige");
		this.cache.CCPStatPrestigeXCookies = l("CCPStatPrestigeXCookies");

		this.cache.CCPStatSucked = l("CCPStatSucked");
		this.cache.CCPStatSuckedR = l("CCPStatSuckedR");

		this.cache.CCPStatHalloween = l("CCPStatHalloween");
		this.cache.CCPStatNoel = l("CCPStatNoel");
		this.cache.CCPStatValentin = l("CCPStatValentin");
		this.cache.CCPStatEgg = l("CCPStatEgg");
		this.cache.CCPStatRareEgg = l("CCPStatRareEgg");
		this.cache.CCPStatGarden = l("CCPStatGarden");

		this.cache.CCPProdBat = l("CCPProdBat");

		// Ajoute un bouton pour tout administrer
		if(!CCPG.CCPPanelButton) {
			var panelButton = CCPT.create("div");
			CCPT.attr(panelButton, {'id': 'CCPPanelButton', 'class': 'button title'});
			panelButton.innerHTML = "CCP";
			CCPT.prepend(l("store"), panelButton);

			CCPG.CCPPanelButton = l("CCPPanelButton");
		}

		// clique sur le bouton de CCP
		CCPT.click(CCPG.CCPPanelButton, this.cliqueCCPButton);
		CCPT.click(CCPG.gameButtons, this.cliqueGameButtons);
		// croix du panneau
		CCPT.click(l('CCPPanelX'), this.cliquePanelX);

		this.refreshPanel();	// Charge une fois le panneau
	};
	this.cliqueCCPButton = function() {
		if(!CCPT.visible(CCPG.CCPPanel)) {
			CCPT.show(CCPG.CCPPanel);
			CCPT.hide(CCPG.rows);
			CCPT.addClass(CCPG.game, 'onCCPMenu');
			CCPT.addClass(CCPG.CCPPanelButton, 'selected');
			PlaySound('snd/clickOn.mp3');
		} else {
			CCPT.hide(CCPG.CCPPanel);
			CCPT.show(CCPG.rows);
			CCPT.removeClass(CCPG.game, 'onCCPMenu');
			CCPT.removeClass(CCPG.CCPPanelButton, 'selected');
			PlaySound('snd/clickOff.mp3');
			this.updateMinigames();
		}
		CCPG.menu.innerHTML = "";
		CCPT.removeClass(CCPG.game, 'onMenu');
		Game.onMenu = '';
		CCPT.removeClass(CCPG.gameButtons, 'selected');
	};
	this.cliqueGameButtons = function() {
		CCPT.show(CCPG.rows);
		CCPT.hide(CCPG.CCPPanel);
		CCPT.removeClass(CCPG.CCPPanelButton, 'selected');
	};
	this.cliquePanelX = function() {
		CCPT.hide(CCPG.CCPPanel);
		CCPT.show(CCPG.rows);
		CCPT.removeClass(CCPG.game, 'onCCPMenu');
		CCPT.removeClass(CCPG.CCPPanelButton, 'selected');
		CCPG.menu.innerHTML = "";
		CCPT.removeClass(CCPG.game, 'onMenu');
		Game.onMenu = '';
		CCPT.removeClass(CCPG.gameButtons, 'selected');
		PlaySound('snd/clickOff.mp3');
		CCPG.self.updateMinigames();
	};
	this.updateMinigames = function() {
		for(var i in Game.Objects) {
			var me=Game.Objects[i];
			if (me.minigame && me.minigame.onResize) me.minigame.onResize();
		}
	};

	this.refreshPanel = function() {
		var
			nombreDor = this.getLuckyBank(),
			nombreDorF = this.getLuckyFrenzyBank(),
			nombreDorS = Beautify(nombreDor),
			nombreDorFS = Beautify(nombreDorF),
			recompenseL = this.getLuckyReward(),
			recompenseMaxL = this.getMaxLuckyReward(),
			recompenseLF = this.getLuckyFrenzyReward(),
			recompenseMaxLF = this.getMaxLuckyFrenzyReward(),
			recompenseLS = Beautify(recompenseL)+" / "+Beautify(recompenseMaxL),
			recompenseLFS = Beautify(recompenseLF)+" / "+Beautify(recompenseMaxLF),

			recompenseS = Beautify(this.getSaisonPopupReward()),

			prestige = this.howMuchPrestige(Game.cookiesReset+Game.cookiesEarned),
			resetPercentIncrease = (prestige - Game.prestige) / Game.prestige * 100 || 0,
			// resetPercentIncrease = prestige - Game.prestige || 0,	// == legacy
			nextPrestigeIn = this.howMuchCookiesToNextPrestige(prestige+1),
			nextPrestigeCost = this.howMuchPrestigeInverse(prestige+1) - this.howMuchPrestigeInverse(prestige),
			cookiesXHC = +this.cache.CCPXPrestige.value || +this.options.xPrestige.value || prestige+1,
			cookiesXHCCookies = this.howMuchCookiesToNextPrestige(cookiesXHC),

			sucked = this.wrinklersSucked(),

			sd = CCPG.self.saisonDrops,

			widthMilieu = parseInt(getComputedStyle && getComputedStyle(this.cache.CCPProdBat.parentNode).width, 10) || 200;

		if(Game.cookies>=nombreDor) {
			nombreDorS = '<span class="text-green">'+nombreDorS+'</span>';
		} else {
			nombreDorS += ' ('+this.formatTime((nombreDor-Game.cookies) / this.effectiveCps, true)+')';
		}
		if(Game.cookies>=nombreDorF) {
			nombreDorFS = '<span class="text-green">'+nombreDorFS+'</span>';
		} else {
			nombreDorFS += ' ('+this.formatTime((nombreDorF-Game.cookies) / this.effectiveCps, true)+')';
		}
		if(recompenseL==recompenseMaxL) {
			recompenseLS = '<span class="text-green">'+recompenseLS+'</span>';
		}
		if(recompenseLF==recompenseMaxLF) {
			recompenseLFS = '<span class="text-green">'+recompenseLFS+'</span>';
		}

		if(cookiesXHCCookies==0) {
			cookiesXHCCookies = '<span class="text-green">Fini ! (total: '+Beautify(this.howMuchPrestigeInverse(cookiesXHC))+')</span>';
		} else {
			cookiesXHCCookies = Beautify(cookiesXHCCookies)+' ('+this.formatTime(cookiesXHCCookies/this.effectiveCps, true)+')';
		}

		this.cache.CCPStatNombreDorL.innerHTML = nombreDorS;
		this.cache.CCPStatNombreDorLF.innerHTML = nombreDorFS;
		this.cache.CCPStatRecompenseL.innerHTML = recompenseLS;
		this.cache.CCPStatRecompenseLF.innerHTML = recompenseLFS;

		if(this.options.cookieDore.value) {
			if(Game.Has('Golden switch [off]') || this.hasShimmerPop("golden")) {
				CCPT.hide(this.cache.CCPStatNextCDDiv);
			} else {
				var widthNextCD = 150,
					widthNextCDFond = widthNextCD*Game.shimmerTypes.golden.minTime/Game.shimmerTypes.golden.maxTime,
					widthNextCDBarre = widthNextCD*Game.shimmerTypes.golden.time/Game.shimmerTypes.golden.maxTime,
					classeNextCDBarre = "background-white";
				if(Game.shimmerTypes.golden.time>=Game.shimmerTypes.golden.minTime) {
					classeNextCDBarre = "background-cyan";
				}
				CCPT.css(this.cache.CCPStatNextCDFond, "width", widthNextCDFond+"px");

				CCPT.css(this.cache.CCPStatNextCDBarre, "margin-left", widthNextCDBarre+"px");
				CCPT.removeClass(this.cache.CCPStatNextCDBarre, "background-white background-cyan");
				CCPT.addClass(this.cache.CCPStatNextCDBarre, classeNextCDBarre);
				this.cache.CCPStatNextCDTemps.innerHTML = this.formatTime((Game.shimmerTypes.golden.maxTime-Game.shimmerTypes.golden.time)/Game.fps);

				CCPT.show(this.cache.CCPStatNextCDDiv);
			}
		}

		this.cache.CCPStatRecompenseS.innerHTML = recompenseS;

		if(this.options.saisonPopup.value) {
			if(Game.season!='christmas' || this.hasShimmerPop("reindeer")) {
				CCPT.hide(this.cache.CCPStatNextSPDiv);
			} else {
				var widthNextSP = 150,
					widthNextSPFond = widthNextSP*Game.shimmerTypes.reindeer.minTime/Game.shimmerTypes.reindeer.maxTime,
					widthNextSPBarre = widthNextSP*Game.shimmerTypes.reindeer.time/Game.shimmerTypes.reindeer.maxTime,
					classeNextSPBarre = "background-white";
				if(Game.shimmerTypes.reindeer.time>=Game.shimmerTypes.reindeer.minTime) {
					classeNextSPBarre = "background-yellow";
				}
				CCPT.css(this.cache.CCPStatNextSPFond, "width", widthNextSPFond+"px");

				CCPT.css(this.cache.CCPStatNextSPBarre, "margin-left", widthNextSPBarre+"px");
				CCPT.removeClass(this.cache.CCPStatNextSPBarre, "background-white background-yellow");
				CCPT.addClass(this.cache.CCPStatNextSPBarre, classeNextSPBarre);
				this.cache.CCPStatNextSPTemps.innerHTML = this.formatTime((Game.shimmerTypes.reindeer.maxTime-Game.shimmerTypes.reindeer.time)/Game.fps);

				CCPT.show(this.cache.CCPStatNextSPDiv);
			}
		}

		this.cache.CCPStatPrestige.innerHTML = Beautify(prestige)+" ("+Beautify(prestige)+"%) soit +"+Beautify(resetPercentIncrease)+"%";
		this.cache.CCPStatPrestigeNextCookies.innerHTML = Beautify(nextPrestigeIn)+" / "+Beautify(nextPrestigeCost);
		this.cache.CCPStatPrestigeNextTime.innerHTML = this.formatTime(nextPrestigeIn/this.effectiveCps);
		this.cache.CCPXPrestige.value = cookiesXHC;
		this.cache.CCPStatPrestigeXCookies.innerHTML = cookiesXHCCookies;

		this.cache.CCPStatSucked.innerHTML = Beautify(sucked[0]);
		this.cache.CCPStatSuckedR.innerHTML = Beautify(sucked[1]);

		this.infosSaisons();
		this.cache.CCPStatHalloween.innerHTML = sd.halloween.text;
		this.cache.CCPStatNoel.innerHTML = sd.noel.text;
		this.cache.CCPStatValentin.innerHTML = sd.valentin.text;
		this.cache.CCPStatEgg.innerHTML = sd.egg.text;
		this.cache.CCPStatRareEgg.innerHTML = sd.rareEgg.text;
		this.cache.CCPStatGarden.innerHTML = sd.garden.text;

		this.cache.CCPProdBat.innerHTML = this.proportionProdBatiments(widthMilieu);

		try {
			if(typeof _refreshPanel === "function") _refreshPanel();
		} catch(ex) {warn("Erreur Addon CCP : "+ex);}
	};

	this.credits = function() {
		return ''
			+ '<div class="section">Updates</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">24/03/2018 - Poisson d\'avril</div>'
			+ '<div class="listing">-v16.0</div>'
			+ '<div class="listing">-Corrections suite à la nouvelle version du jeu</div>'
			+ '<div class="listing">-Ajout d\'une option pour acheter tous les upgrades disponibles</div>'
			+ '<div class="listing">-Ajout d\'un cache pour la gestion interne</div>'
			+ '<div class="listing">-Optimisations et correction de bugs</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">01/10/2017 - Enfin libre !</div>'
			+ '<div class="listing">-v15.0</div>'
			+ '<div class="listing">-Supression de jQuery</div>'
			+ '<div class="listing">-Script en mode strict</div>'
			+ '<div class="listing">-Ajout des types d\'options link et password</div>'
			+ '<div class="listing">-Ajout d\'une gestion des classes, DOM et évènements</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">04/06/2017 - Synergie à tout prix</div>'
			+ '<div class="listing">-v14.1</div>'
			+ '<div class="listing">-Correction des calculs de BCI pour la synergie</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">28/08/2017 - Le futur, c\'est maintenant</div>'
			+ '<div class="listing">-v14.0</div>'
			+ '<div class="listing">-Ajout d\'une fonctionnalité de tâches Cron</div>'
			+ '<div class="listing">-Ajout d\'une fonction pour les addons</div>'
			+ '<div class="listing">-Ajout d\'une fonction Ajax intégré</div>'
			+ '<div class="listing">-Ajout BCI des nouveaux upgrades</div>'
			+ '<div class="listing">-Ajout BCI du lait</div>'
			+ '<div class="listing">-Ajout BCI pour la synergie des grandmas</div>'
			+ '<div class="listing">-Ajout BCI pour la synergie des bâtiments</div>'
			+ '<div class="listing">-Ajout de notification lors du clique sur un cookie</div>'
			+ '<div class="listing">-Ajout d\'une option pour ne pas tuer les larves brillantes</div>'
			+ '<div class="listing">-Ajout des sons du jeu</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">21/08/2017 - Déjà 4 ans</div>'
			+ '<div class="listing">-v13.0</div>'
			+ '<div class="listing">-correction du fonctionnement des cookies dorés et des rennes</div>'
			+ '<div class="listing">-correction de l\'affichage du panneau CCP</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">28/03/2016 - Les Quatre Saisons</div>'
			+ '<div class="listing">-v12.2</div>'
			+ '<div class="listing">-ajout des infos sur les drops des saisons</div>'
			+ '<div class="listing">-ajout d\'une option pour ne pas être alerté des cookies rouges</div>'
			+ '<div class="listing">-optimisations et correction de bugs</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">22/03/2016 - Chenille 2.0</div>'
			+ '<div class="listing">-v11.0</div>'
			+ '<div class="listing">-utilisation du volume du jeu sur les sons</div>'
			+ '<div class="listing">-remise en marche des infos sur les upgrades</div>'
			+ '<div class="listing">-correction de bugs</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">06/03/2016 - C\'est la chenille qui redémarre</div>'
			+ '<div class="listing">-v10.0</div>'
			+ '<div class="listing">-passage en UTF-8</div>'
			+ '<div class="listing">-nouveau système d\'achat de bâtiments</div>'
			+ '<div class="listing">-nouveau système de prestige</div>'
			+ '<div class="listing">-nouveau design</div>'
			+ '<div class="listing">-optimisations et correction de bugs</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">23/03/2015 - C\'est l\'heure des emplettes</div>'
			+ '<div class="listing">-v9.0</div>'
			+ '<div class="listing">-BCI sur les upgrades</div>'
			+ '<div class="listing">-ajout d\'une bouton pour vérifier si une version est disponible</div>'
			+ '<div class="listing">-changement du timing pour le contrôle de version</div>'
			+ '<div class="listing">-amélioration compatibilité avec la beta</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">20/03/2015 - éclipse solaire</div>'
			+ '<div class="listing">-v8.0</div>'
			+ '<div class="listing">-correction de bugs</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">12/11/2014 - engagé Baleine</div>'
			+ '<div class="listing">-v7.0</div>'
			+ '<div class="listing">-mise à jour très en retard de la version de compatibilité</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">22/10/2014 - il y en a pour tout le monde</div>'
			+ '<div class="listing">-v6.0</div>'
			+ '<div class="listing">-ajout d\'une option dans Divers pour le placement du bouton "CCP"</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">16/10/2014 - je vous ai compris</div>'
			+ '<div class="listing">-v5.0</div>'
			+ '<div class="listing">-changements suite à la demande générale</div>'
			+ '<div class="listing">-nettoyage</div>'
			+ '<div class="listing">-diverses optimisations et corrections</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">15/10/2014 - la mort en face</div>'
			+ '<div class="listing">-v4</div>'
			+ '<div class="listing">-prise en compte de la future version du jeu</div>'
			+ '<div class="listing">-déplace le bouton du menu CCP</div>'
			+ '<div class="listing">-notifications sur les MAJ</div>'
			+ '<div class="listing">-diverses optimisations... encore</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">09/10/2014 - on arrange le slide</div>'
			+ '<div class="listing">-v3.1</div>'
			+ '<div class="listing">-correction d\'un petit bug</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">09/10/2014 - des cookies sur le bureau</div>'
			+ '<div class="listing">-v3.0</div>'
			+ '<div class="listing">-notifications hors navigateur</div>'
			+ '<div class="listing">-options de type "durée" modifiables</div>'
			+ '<div class="listing">-optimisation du contrôle des cookies dorés/rennes</div>'
			+ '<div class="listing">-diverses optimisations</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">07/10/2014 - la bonne cacahuète</div>'
			+ '<div class="listing">-v2.0</div>'
			+ '<div class="listing">-options facile dans les addons</div>'
			+ '<div class="listing">-changement de la numérotation des versions</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">06/10/2014 - faire briller les chromes</div>'
			+ '<div class="listing">-v1.9</div>'
			+ '<div class="listing">-ajout des notes d\'updates</div>'
			+ '<div class="listing">-options dans le bon ordre</div>'
			+ '<div class="listing">-corrections sur la vérification de maj</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">19/09/2014 - personnalisations</div>'
			+ '<div class="listing">-v1.8</div>'
			+ '<div class="listing">-possibilité de mettre des Addons</div>'
			+ '<div class="listing">-changement de la boucle principal pour être moins "gourmand"</div>'
			+ '<div class="listing">-mise en cache des couleurs des bâtiments</div>'
			+ '<div class="listing">-mise en cache de fonctions de recherches</div>'
			+ '<div class="listing">-nettoyages divers</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">12/09/2014 - trou noir</div>'
			+ '<div class="listing">-v1.7</div>'
			+ '<div class="listing">-aucune idée de son existence</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">12/09/2014 - toujours au top</div>'
			+ '<div class="listing">-v1.6</div>'
			+ '<div class="listing">-ajout d\'une vérification de maj</div>'
			+ '<div class="listing">-nettoyages divers</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">03/09/2014 - Faut s\'appliquer</div>'
			+ '<div class="listing">-v1.5</div>'
			+ '<div class="listing">-Correction de bugs</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">03/09/2014 - annihilation</div>'
			+ '<div class="listing">-v1.4</div>'
			+ '<div class="listing">-destruction plus complète en quittant</div>'
			+ '<div class="listing">-ajout d\'options pour quitter</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">03/09/2014 - CCP est mort, vive CCP</div>'
			+ '<div class="listing">-v1.3</div>'
			+ '<div class="listing">-possibilité de recharger CCP</div>'
			+ '<div class="listing">-possibilité de quitter CCP</div>'
			+ '</div>'

			+ '<div class="subsection update small">'
			+ '<div class="title">02/09/2014 - cache cache</div>'
			+ '<div class="listing">-v1.2</div>'
			+ '<div class="listing">-nombres supérieur à 1e21 plus jolis</div>'
			+ '<div class="listing">-résolutions de problèmes de cache client</div>'
			+ '<div class="listing">-nettoyages divers</div>'
			+ '</div>'

			+ '<div class="subsection update">'
			+ '<div class="title">12/04/2014 - félicitation, c\'est un plugin</div>'
			+ '<div class="listing">-v1.0 et v1.1</div>'
			+ '<div class="listing">-créations diverses et variées qui ont beaucoup évoluées</div>'
			+ '<div class="listing">-suivi des cookies dorées</div>'
			+ '<div class="listing">-suivi des saisons</div>'
			+ '<div class="listing">-suivi des bâtiments</div>'
			+ '<div class="listing">-suivi du prestige</div>'
			+ '<div class="listing">-optimisations diverses</div>'
			+ '</div>'
			;
	};

	this.boutonWrinklers = function() {
		// affiche un bouton pour tuer toutes les larves
		var bt = CCPT.create("button");
		CCPT.attr(bt, {'id': 'CCPPopWrinklers', 'type': 'button'});
		bt.innerHTML = 'Pop all Wrinklers';
		l("cookieAnchor").appendChild(bt);

		// affiche/cache le bouton en fonction du nombre de larves
		CCPT.hover(l("sectionLeft"), this.boutonWrinklersIn, this.boutonWrinklersOut);
		CCPT.click(l('CCPPopWrinklers'), function() {
			if(CCPG.self.options.preserveShiny.value) {
				for(var i in Game.wrinklers) {
					if(Game.wrinklers[i].type!=1) {
						Game.wrinklers[i].hp=0;
					}
				}
			} else {
				Game.CollectWrinklers();
			}
			CCPG.self.boutonWrinklersOut();
		});
	};
	this.boutonWrinklersIn = function() {
		if(CCPG.self.wrinklersExist() && CCPG.self.options.popWrinklers.value) {
			CCPT.fadeIn(l('CCPPopWrinklers'), 200);
		}
	};
	this.boutonWrinklersOut = function() {
		CCPT.fadeOut(l('CCPPopWrinklers'), 200);
	};

	this.initCleanUI = function() {
		document.addEventListener('mousemove', this.corrigerMouseY);
	};
	// corriger les coordonnées Y de la souris lorsqu'il n'y a pas la barre supérieur du jeu
	this.corrigerMouseY = function() {
		if(CCPG.self.options.cleanUI.value) {
			Game.mouseY+=32;
		}
	};
	this.cleanUI = function() {
		if(this.options.cleanUI.value) {
			CCPT.addClass(document.body, "cleanUI");
		} else {
			CCPT.removeClass(document.body, "cleanUI");
		}

		// Recalcule les dimensions du canvas d'arrière plan
		function recalculateCanvasDimensions() {
			Game.Background.canvas.height=Game.Background.canvas.parentNode.offsetHeight;
			Game.LeftBackground.canvas.height=Game.LeftBackground.canvas.parentNode.offsetHeight;
		}
		// On attend un peu au cas où le CSS n'a pas encore été parsé :(
		setTimeout(recalculateCanvasDimensions, 1000);
	};

	this.boutonCCPFixe = function() {
		if(this.options.boutonCCPFixe.value) {
			CCPT.addClass(document.body, "boutonCCPFixe");
		} else {
			CCPT.removeClass(document.body, "boutonCCPFixe");
		}
	};
	this.switchPwd = function(optid) {
		var opt = l(optid);
		opt.type = (opt.type==="password"?"":"password");
	};

	this.infosUpgrade = function() {
		if(!this.backup["Game.RebuildUpgrades"]) {
			this.backup["Game.RebuildUpgrades"] = eval("("+Game.RebuildUpgrades.toString()+")");
		}
		Game.RebuildUpgrades = this.appendToNative(Game.RebuildUpgrades, function() {
			for(var i in Game.UpgradesInStore) {
				// Game.getDynamicTooltip
				if(l('upgrade'+i).onmouseover) {
					l('upgrade'+i).onmouseover = CCPG.self.onmouseoverUpgrade(Game.UpgradesInStore[i]);
				}
			}
		});
		Game.RebuildUpgrades();
	};

	this.onmouseoverUpgrade = function(el) {
		return function() {
			try {
				if(!Game.mouseDown) {
					Game.setOnCrate(this);
					Game.tooltip.dynamic=1;
					Game.tooltip.draw(this, CCPG.self.tooltipUpgrade(el), 'store');
					Game.tooltip.wobble();
				}
			} catch(ex) {warn("Fail kill infosUpgrade : "+ex);}
		};
	};

	this.tooltipUpgrade = function(el) {
		var param = Game.crate(el,'store',undefined,undefined,1);
		if(typeof param === "function") {
			return function() {
				try {
					if(CCPG.self.options.infosUpgrade.value) {
						var price = el.getPrice(),
							bonus = CCPG.self.getProdBonusUpgrade(el),
							pourcent = Math.floor(100*bonus/Game.cookiesPs),
							tempsRest = '';

						if(Game.cookies>=price) {
							tempsRest = '<span class="text-green">Fini !</span>';
						} else {
							tempsRest = CCPG.self.formatTime((price-Game.cookies) / CCPG.self.effectiveCps, true);
						}

						var cppTooltip = ''
							+ '<div class="line"></div>'
							+ '<div class="description">'
							+ '&bull; Bonus : <b>'+Beautify(bonus, 2)+(pourcent>0?' (+'+Beautify(pourcent)+'%)':'')
							+ ' environ'	// calculs encore incomplets
							+ '</b><br/>'
							+ (CCPDebug?'&bull; New Prod : <b>'+Beautify(bonus+Game.cookiesPs, 2)+'</b><br/>':'')
							+ '&bull; BCI : <b>'+Beautify(price/bonus, 1)+'</b><br/>'
							+ '&bull; Temps restant : <b>'+tempsRest+'</b><br/>'
							+ '</div>'
							;

						return param() + cppTooltip;
					} else {
						return param();
					}
				} catch(ex) {
					warn("Fail kill tooltipUpgrade : "+ex);
					return param();
				}
			};
		}
		return param;
	};

	this.infosObject = function() {
		for(var i in Game.ObjectsById) {
			var el=Game.ObjectsById[i];
			this.backup["Game.ObjectsById["+i+"].tooltip"] = el.tooltip;
			el.tooltip = this.appendToThisNative(el, el.tooltip, function(param, el) {
				if(CCPG.self.options.infosObject.value) {
					// si objet débloqué
					if(!el.locked) {
						var price = el.getSumPrice(Game.buyBulk),
							bonus = CCPG.self.getProdBonusObject(el, el.amount+Game.buyBulk),
							pourcent = Math.floor(100*bonus/Game.cookiesPs),
							tempsRest = '';

						if(Game.cookies>=price) {
							tempsRest = '<span class="text-green">Fini !</span>';
						} else {
							tempsRest = CCPG.self.formatTime((price-Game.cookies) / CCPG.self.effectiveCps, true);
						}

						var cppTooltip = ''
							+ '<div class="line"></div>'
							+ '<div class="description">'
							+ '&bull; Bonus : <b>'+Beautify(bonus, 2)+(pourcent>0?' (+'+Beautify(pourcent)+'%)':'')+'</b><br/>'
							+ (CCPDebug?'&bull; New Prod : <b>'+Beautify(bonus+Game.cookiesPs, 2)+'</b><br/>':'')
							+ '&bull; BCI : <b>'+Beautify(price/bonus, 1)+'</b><br/>'
							+ '&bull; Temps restant : <b>'+tempsRest+'</b><br/>'
							+ '</div>'
							;

						if(CCPG.self.options.deficitLuck.value) {
							var nbL = CCPG.self.getLuckyBank()-(Game.cookies-price),
								nbF = CCPG.self.getLuckyFrenzyBank()-(Game.cookies-price);
							if(nbL>0 || nbF>0) {
								cppTooltip += '<div class="description">';
								if(nbL>0) {
									cppTooltip += '<span class="text-yellow">&bull; Lucky : '+CCPG.self.formatTime(nbL/CCPG.self.effectiveCps, true)+'</span><br/>';
								}
								cppTooltip += '<span class="text-red">&bull; Lucky+Frenzy : '+CCPG.self.formatTime(nbF/CCPG.self.effectiveCps, true)+'</span><br/>';
								cppTooltip += '</div>';
							}
						}

						return param + cppTooltip;
					} else {	// si bloqué, on affiche que le temps restant pour l'acheter
						var cppTooltip = ''
							+ '<div class="description">'
							+ '&bull; Temps restant : <b>'+CCPG.self.formatTime((el.getPrice()-Game.cookies) / CCPG.self.effectiveCps, true)+'</b><br/>'
							+ '</div>'
							;

						return param + cppTooltip;
					}
				}
			});
		}
	};

	/* this.corrigeBugs = function() {
		for(var i in Game.ObjectsById) {
			var el=Game.ObjectsById[i];
			// corrige les soucis d'arrondis quand on veux acheter plusieurs bâtiments
			this.backup["Game.ObjectsById["+i+"].getSumPrice"] = el.getSumPrice;
			el.getSumPrice=function(amount)//return how much it would cost to buy [amount] more of this building
			{
				var price=0;
				for (var i=Math.max(0,el.amount);i<Math.max(0,(el.amount)+amount);i++)
				{
					price+=Math.ceil(el.basePrice*Math.pow(Game.priceIncrease,Math.max(0,i-el.free)));
				}
				if (Game.Has('Season savings')) price*=0.99;
				if (Game.Has('Santa\'s dominion')) price*=0.99;
				if (Game.Has('Faberge egg')) price*=0.99;
				if (Game.Has('Divine discount')) price*=0.99;
				if (Game.hasAura('Fierce Hoarder')) price*=0.98;
				return Math.ceil(price);
			};
			// corrige les soucis d'arrondis quand on veux vendre plusieurs bâtiments
			this.backup["Game.ObjectsById["+i+"].getReverseSumPrice"] = el.getReverseSumPrice;
			el.getReverseSumPrice=function(amount)//return how much you'd get from selling [amount] of this building
			{
				var price=0;
				for (var i=Math.max(0,(el.amount)-amount);i<Math.max(0,el.amount);i++)
				{
					price+=Math.ceil(el.basePrice*Math.pow(Game.priceIncrease,Math.max(0,i-el.free)));
				}
				if (Game.Has('Season savings')) price*=0.99;
				if (Game.Has('Santa\'s dominion')) price*=0.99;
				if (Game.Has('Faberge egg')) price*=0.99;
				if (Game.Has('Divine discount')) price*=0.99;
				if (Game.hasAura('Fierce Hoarder')) price*=0.98;
				price*=el.getSellMultiplier();
				return Math.ceil(price);
			};
		}
	}; */

	this.initColorPrices = function() {
		for(var i in Game.ObjectsById) {
			var el=Game.ObjectsById[i];
			this.backup["Game.ObjectsById["+i+"].buy"] = el.buy;
			el.buy = this.appendToThisNative(el, el.buy, function() {
				// Game.upgradesToRebuild=1;
				CCPG.self.colorPrices();
			});
			this.backup["Game.ObjectsById["+i+"].sell"] = el.sell;
			el.sell = this.appendToThisNative(el, el.sell, function() {
				// Game.upgradesToRebuild=1;
				CCPG.self.colorPrices();
			});
		}
		for(var i in Game.UpgradesById) {
			var el=Game.UpgradesById[i];
			this.backup["Game.UpgradesById["+i+"].buy"] = el.buy;
			el.buy = this.appendToThisNative(el, el.buy, function() {
				setTimeout(function() {
					CCPG.self.colorPrices();
				}, 500);	// attendre car sinon upgrade non pris en compte
			});
			this.backup["Game.UpgradesById["+i+"].earn"] = el.earn;
			el.earn = this.appendToThisNative(el, el.earn, function() {
				setTimeout(function() {
					CCPG.self.colorPrices();
				}, 500);	// attendre car sinon upgrade non pris en compte
			});
		}
		this.cache.productPrice = document.querySelectorAll(".product .price");
	};
	this.colorPrices = function() {
		// nettoie les couleurs
		CCPT.removeClass(this.cache.productPrice, "text-green text-yellow text-orange text-red");

		if(this.options.colorPrices.value) {
			var listeBCI = [];
			for(var i in Game.ObjectsById) {
				var el=Game.ObjectsById[i];
				// if(!el.locked) {
					listeBCI[i] = el.getPrice()/this.getProdBonusObject(el);
				// }
			}

			var max = CCPT.maxArray(listeBCI),
				min = CCPT.minArray(listeBCI),
				moy = (max+min)/2;

			for(var i in Game.ObjectsById) {
				var el=Game.ObjectsById[i];
				// if(!el.locked) {
					var classe = "text-yellow";		// défaut
					if(listeBCI[i]==min) classe = "text-green";	// mini
					else if(listeBCI[i]==max) classe = "text-red";	// maxi
					else if(listeBCI[i]>moy) classe = "text-orange";	// plus proche du maxi que du mini

					CCPT.addClass(this.cache.productPrice[el.id], classe);
				// }
			}
		}
	};

	this.creerFlash = function() {
		var flash = CCPT.create("div");
		CCPT.attr(flash, "id", "CCPFlash");
		document.body.appendChild(flash);
		this.cache.CCPFlash = flash;
	};
	this.flash = function() {
		CCPT.show(this.cache.CCPFlash);
		CCPT.fadeOut(this.cache.CCPFlash, 400);
	};

	this.achatUpgrades = function() {
		for(var i in Game.UpgradesInStore) {
			var me = Game.UpgradesInStore[i];
			if(me.type=="upgrade" && (me.pool=="" || me.pool=="cookie")) {
				me.buy();
			}
		}
	};

	this.recolterMature = function() {
		if(Game.isMinigameReady(Game.Objects['Farm'])) {
			var M = Game.Objects['Farm'].minigame;
			for (var i in M.plants) {
				M.harvestAll(M.plants[i]);
			}
		}
	};

	this.proportionProdBatiments = function(maxWidth) {
		maxWidth = maxWidth || 800;
		var total = 0,
			prods = [],
			str = '';

		for(var i in Game.Objects) {
			var obj = Game.Objects[i];
			var prodObj = Game.cookiesPsByType[obj.name];
			prods.push({name: obj.name, prod: prodObj});
			total+=prodObj;
		}

		for(var i in prods) {
			var w = prods[i].prod/total*maxWidth;
			var h = i/prods.length;
			h+=0.8;
			h*=1.75;
			var r = Math.sin(Math.PI*h)/2+0.5,
				g = Math.sin(Math.PI*(h + 2/3))/2+0.5,
				b = Math.sin(Math.PI*(h + 4/3))/2+0.5;

			str+='<div class="batimentBarre" style="width:'+w+'px;background-color:rgb('+Math.floor(r*255)+','+Math.floor(g*255)+','+Math.floor(b*255)+');"'
				+ Game.getTooltip(
					'<div class="batimentBarreTooltip"><b>'+prods[i].name+' :</b> '+Beautify(prods[i].prod, 2)+'</div>'
				, 'this') + '></div>';
		}
		return str;
	};

	/*=====================================================================================
	SHIMMERS (COOKIE DORE & SUCH)
	=======================================================================================*/
	this.initShimmer = function() {
		if(!this.backup["Game.shimmerTypes['golden'].popFunc"]) {
			this.backup["Game.shimmerTypes['golden'].popFunc"] = eval("("+Game.shimmerTypes['golden'].popFunc.toString()+")");
		}

		Game.shimmerTypes['golden'].popFunc = this.appendToThisNative(Game.shimmerTypes['golden'], Game.shimmerTypes['golden'].popFunc, function() {
			if(CCPG.self.options.cookieClicNotif.value) {
				var goldenCookieChoices = {
						"frenzy": "Frenzy",
						"multiply cookies": "Lucky!",
						"ruin cookies": "Ruin!",
						"blood frenzy": "Elder frenzy",
						"clot": "Clot",
						"click frenzy": "Click frenzy",
						"cursed finger": "Cursed finger",
						"chain cookie": "Cookie chain",
						"cookie storm": "Cookie storm",
						"building special": "Building special",
						"dragon harvest": "Dragon Harvest",
						"dragonflight": "Dragonflight",
						"everything must go": "Everything must go",
						"free sugar lump": "Sweet",
						"blab": "???",
					},
					desc = goldenCookieChoices[Game.shimmerTypes['golden'].last] || '';

				if(desc!="") {
					CCPG.self.notifAdd(new CCPN('Cookie doré cliqué', desc, "cookieClicNotif"));
				} else {
					warn("Erreur Notification Cookie : "+Game.shimmerTypes['golden'].last);
				}
			}
		});
	};
	this.shimmerLoop = function() {
		var pops = this.hasShimmerPop();
		if(pops.golden) {
			for(var i in pops.golden.list) {
				var me = pops.golden.list[i];

				if(!this.dejaShimmer[me.id]) {
					if(!(this.options.cookieAlerteRed.value && me.wrath)) {
						if(this.options.cookieFlash.value) {
							this.flash();
						}
						if(this.options.cookieAudio.value) {
							this.jouerAudio();
						}
						if(this.options.cookieNotif.value) {
							this.notifAdd(new CCPN("Cookie doré", "Un cookie doré sauvage vient d'apparaitre.", "cookieNotif"));
						}
					}
				}
				this.dejaShimmer[me.id] = true;
			}
		}
		if(pops.reindeer) {
			for(var i in pops.reindeer.list) {
				var me = pops.reindeer.list[i];

				if(!this.dejaShimmer[me.id]) {
					if(this.options.saisonFlash.value) {
						this.flash();
					}
					if(this.options.saisonAudio.value) {
						this.jouerAudio(true);
					}
					if(this.options.saisonNotif.value) {
						this.notifAdd(new CCPN("Renne", "Un renne sauvage vient d'apparaitre.", "saisonNotif"));
					}
				}
				this.dejaShimmer[me.id] = true;
			}
		}

		// purge des shimmers déjà disparus
		for(var i in this.dejaShimmer) {
			var toujoursVivant = Game.shimmers.find(function(me) {
				return me.id === +i;
			});
			if(!toujoursVivant) {
				// this.dejaShimmer[i] = false;
				this.dejaShimmer.splice(i, 1);	// économie de mémoire
			}
		}
	};

	this.hasShimmerPop = function(type) {
		var pop = {};
		for(var i in Game.shimmers) {
			var me = Game.shimmers[i];
			if(me.spawnLead && me.life>0) {
				if(!pop[me.type]) pop[me.type] = {nb: 0, list: []};
				pop[me.type].nb++;
				pop[me.type].list.push({
					id: me.id,
					wrath: me.wrath,
				});
			}
		}
		return (type?pop[type].nb:pop);
	};

	/*=====================================================================================
	AUDIO
	=======================================================================================*/
	this.initAudios = function() {
		var audioCSrc = this.options.cookieAudioSrc.value.trim() || this.audioCSrcDef,
			audioSSrc = this.options.saisonAudioSrc.value.trim() || this.audioSSrcDef,

			desc = "Si vous utilisez un son personnalisé, vérifiez si l'URL est valide.";

		this.cache.CCPAudioC = new Audio(audioCSrc);	// cookie
		this.cache.CCPAudioC.volume = Game.volume/100 || 1;
		this.cache.CCPAudioC.onerror = function() {
			alert("<b>Erreur</b> : impossible de charger le son des cookies dorés.", desc, [11,6]);
		};
		this.cache.CCPAudioC.onended = function() {	// rembobine
			if(window.chrome) {	// bug chrome ?
				CCPG.self.cache.CCPAudioC.load();
			} else if(navigator.appName!='Microsoft Internet Explorer') {	// IE10 on rembobine pas
				CCPG.self.cache.CCPAudioC.currentTime=0.0;
				// CCPG.self.cache.CCPAudioC.pause();
			}
		};

		this.cache.CCPAudioS = new Audio(audioSSrc);	// saison
		this.cache.CCPAudioS.volume = Game.volume/100 || 1;
		this.cache.CCPAudioS.onerror = function() {
			alert("<b>Erreur</b> : impossible de charger le son des rennes.", desc, [11,6]);
		};
		this.cache.CCPAudioS.onended = function() {	// rembobine
			if(window.chrome) {	// bug chrome ?
				CCPG.self.cache.CCPAudioS.load();
			} else if(navigator.appName!='Microsoft Internet Explorer') {	// IE10 on rembobine pas
				CCPG.self.cache.CCPAudioS.currentTime=0.0;
				// CCPG.self.cache.CCPAudioS.pause();
			}
		};
		this.volume();
	};
	this.jouerAudio = function(CDOuSP) {
		if(!CDOuSP) {
			if(this.cache.CCPAudioC.networkState==1) {	// OK
				this.cache.CCPAudioC.play();	// cookie
			}
		} else {
			if(this.cache.CCPAudioS.networkState==1) {	// OK
				this.cache.CCPAudioS.play();	// saison
			}
		}
	};
	this.volume = function() {
		if(!this.backup["Game.setVolume"]) {
			this.backup["Game.setVolume"] = eval("("+Game.setVolume.toString()+")");
		}
		Game.setVolume = this.appendToNative(Game.setVolume, function() {
			CCPG.self.cache.CCPAudioC.volume = Game.volume/100 || 1;
			CCPG.self.cache.CCPAudioS.volume = Game.volume/100 || 1;
		});
	};

	/*=====================================================================================
	NOTIFICATIONS
	=======================================================================================*/
	this.notifActif = function() {
		return this.options.cookieNotif.value
			|| this.options.saisonNotif.value
			|| this.options.checkVersionNotif.value;
	};
	this.hasNotif = function() {
		return this.notifQueue.length>0;
	};
	this.notifGrant = function() {
		try {	// pas encore standard partout
			if(this.notifActif()) {
				Notification.requestPermission(function(status) {
					if(CCPDebug) log("Notification Permission : "+status);
				});
			}
		} catch(ex) {warn("Erreur Notification : "+ex);}
	};
	this.notifAdd = function(ccpNotif) {
		if(this.notifActif()) {
			// pas plus de 10 en même temps
			if(this.notifQueue.length>10) this.notifQueue.shift();
			this.notifQueue.push(ccpNotif);
		}
	};
	this.notifShow = function() {
		try {	// pas encore standard partout
			switch(Notification.permission) {
				case "granted" :	// on a la permission, on affiche la notification
					// afficher la queue
					while(this.hasNotif()) {
						var i = this.notifQueue.shift();
						var n = new Notification(i.titre, {
							lang: "fr",
							body: i.body,
							tag: i.tag,
							icon: this.host+"ccp-logo.png",
						});
						n.onclick = function() {window.focus();};	// focus sur l'onglet
						setTimeout(function() {n.close();}, 6000);	// fermeture auto au bout de 6 seconde
					}
				break;
				// case "denied" :	// on n'a pas la permission, tant pis :'(
				// break;
				case "default" :	// aucune info, on demande la permission
				default :
					this.notifGrant();
				break;
			}
		} catch(ex) {warn("Erreur Notification : "+ex);}
	};

	/*=====================================================================================
	DRAW
	=======================================================================================*/
	this.initDraw = function() {
		if(CCPDebug) {
			if(!this.backup["Game.DrawWrinklers"]) {
				this.backup["Game.DrawWrinklers"] = eval("("+Game.DrawWrinklers.toString()+")");
			}
			Game.DrawWrinklers = this.appendToNative(Game.DrawWrinklers, function() {
				for(var i in Game.wrinklers) {
					var me=Game.wrinklers[i];
					if(me.phase>0) {
						Game.LeftBackground.save();
						Game.LeftBackground.globalAlpha=me.close;
						Game.LeftBackground.translate(me.x,me.y);
						Game.LeftBackground.rotate(-(me.r)*Math.PI/180);

						// Game.LeftBackground.strokeStyle="red";
						Game.LeftBackground.strokeRect(-50,-10,100,200);
						Game.LeftBackground.fillText(''+(Math.round(me.hp*10)/10),0,0);

						Game.LeftBackground.restore();
					}
				}
			});

			l("goldenCookie").style.border="1px solid #ffff34";
			l("seasonPopup").style.border="1px solid #2affff";
		}
	};

	/*=====================================================================================
	SAUVEGARDE/CHARGEMENT
	=======================================================================================*/
	this.save = function(manuel) {
		if(manuel) {
			var self = this;
		} else {
			var self = CCPG.self;
		}
		var str = "version:"+self.version+"|",
			opt = self.options;
		for(var i in opt) {
			if(opt[i].type!="buttonSub" && opt[i].type!="buttonLink") {
				str += opt[i].name+":"+opt[i].value+"|";
			}
		}

		str = utf8_to_b64(str)+'!END!';
		str = escape(str);

		if(Game.useLocalStorage) {
			window.localStorage.setItem(self.saveTo,str);
			if(manuel) {
				if(!window.localStorage.getItem(self.saveTo)) {
					alert("Un soucis à eu lieu lors de la sauvegarde", "", "", 1);
				} else {
					alert("Plugin sauvegardé", "", "", 1);
				}
			}
		} else {
			var duree = new Date();
			duree.setFullYear(duree.getFullYear() + 5);
			document.cookie = self.saveTo+'='+str+';expires='+duree.toGMTString()+';';
			if(manuel) {
				if(document.cookie.indexOf(self.saveTo) === -1) {
					alert("Un soucis à eu lieu lors de la sauvegarde", "", "", 1);
				} else {
					alert("Plugin sauvegardé", "", "", 1);
				}
			}
		}
		if(CCPDebug) log(str);
	};

	this.load = function(manuel) {
		if(manuel) {
			var self = this;
		} else {
			var self = CCPG.self;
		}
		var str = "",
			opt = self.options;

		if(Game.useLocalStorage) {
			var localStorage=window.localStorage.getItem(self.saveTo);
			if(!localStorage) {
				if(document.cookie.indexOf(self.saveTo)>=0) str=document.cookie.split(self.saveTo+'=')[1];
			} else {
				str=localStorage;
			}
		} else {
			if(document.cookie.indexOf(self.saveTo)>=0) str=document.cookie.split(self.saveTo+'=')[1];
		}

		str = unescape(str);
		str = str.split('!END!')[0];
		str = b64_to_utf8(str);

		var strS = str.split("|");
		for(var i=0, ll=strS.length; i<ll; ++i) {
			var strSS = strS[i].split(":"),
				name = strSS[0],
				value = strSS[1],
				versionSauv = 0;
			if(value!==undefined) {
				if(name=="version") {
					versionSauv = +value;
				} else if(opt[name]) {

					if(name=="checkVersionRate" && versionSauv<=8) {	// rétro compatibilité à la version 8 (non vital)
						if(+value>3600000) value = 3600000;
					}

					switch(opt[name].type) {
						case "inputStr" :
						case "inputAud" :
						case "inputPwd" :
							opt[name].update(value);
						break;
						default :
							opt[name].update(+value);
						break;
					}
				// } else {
					// if(CCPDebug) log("? "+name+":"+value);
				}
			}
		}

		if(manuel) {
			alert("Plugin chargé", "", "", 1);
		}
		if(CCPDebug) log(str);
	};

	this.reset = function(manuel) {
		if(manuel) {
			var self = this;
		} else {
			var self = CCPG.self;
		}
		var str= "";
		str = utf8_to_b64(str)+'!END!';
		str = escape(str);

		if(Game.useLocalStorage) {
			window.localStorage.setItem(self.saveTo,str);
			if(manuel) {
				if(!window.localStorage.getItem(self.saveTo)) {
					alert("Un soucis à eu lieu lors du reset de la sauvegarde", "", "", 1);
				} else {
					alert("Sauvegarde remise à zéro", "", "", 1);
				}
			}
		} else {
			var duree = new Date();
			duree.setFullYear(duree.getFullYear() + 5);
			document.cookie = self.saveTo+'='+str+';expires='+duree.toGMTString()+';';
			if(manuel) {
				if(document.cookie.indexOf(self.saveTo) === -1) {
					alert("Un soucis à eu lieu lors du reset de la sauvegarde", "", "", 1);
				} else {
					alert("Sauvegarde remise à zéro", "", "", 1);
				}
			}
		}
		if(CCPDebug) log(str);
	};

	/*=====================================================================================
	MAIN LOOP
	=======================================================================================*/
	this.mainLoop = function() {
		Timer.say('PLUGIN');
		Timer.track('CCP browser stuff');
		// Timer.clean();

		if(!Game.sesame || document.hasFocus() || Game.prefs.focus || Game.loopT%10==0) {

			this.baseCps = this.getBaseCps();
			this.effectiveCps = this.getEffectiveCps();
			Timer.track('CCP init');

			try {
				if(typeof _mainLoop === "function") _mainLoop();
				Timer.track('CCP addon');
			} catch(ex) {warn("Erreur Addon CCP : "+ex);}

			this.shimmerLoop();

			if(this.notifActif() && this.hasNotif()) {
				this.notifShow();	// affiche la liste des notifications
			}
			Timer.track('CCP alertes');

			if(CCPT.visible(CCPG.CCPPanel)) {
				this.refreshPanel();
			}
			Timer.track('CCP panel');

		}

		Timer.track('CCP loop');
		// Timer.clean();
		// Timer.say('ENDPLUGIN');
		if(Game.sesame) {
			this.mainLoopTimeout = setTimeout(function() {CCPG.self.mainLoop();}, 1000/Game.fps);
		} else {
			this.mainLoopTimeout = setTimeout(function() {CCPG.self.mainLoop();}, this.options.refreshRate.value);
		}
	};

	this.init = function() {
		// Petit message au cas où il y ai un soucis après une maj
		if(this.compatibleWith<Game.version) {
			alert("<b>ATTENTION !</b> La dernière version compatible du jeu est la "+this.compatibleWith+" !", "Il se peut que le plugin ne fonctionne pas correctement avec cette mise à jour.", [11,6]);
		}

		// Avant de charger les paramètres
		this.creerFlash();
		this.creerPanel();
		this.boutonWrinklers();
		this.initColorPrices();
		// this.corrigeBugs();
		this.initCleanUI();

		this.load(true);

		// Après avoir chargé les paramètres
		// this.cleanUI();
		this.infosObject();
		this.infosUpgrade();
		// this.colorPrices();
		// this.initAudios();
		this.initShimmer();
		this.initDraw();

		try {
			if(typeof _init === "function") _init();
		} catch(ex) {warn("Erreur Addon CCP : "+ex);}

		this.checkVersionLoop();

		// this.centuryEggInterval = setInterval(function() {if(Game.Has('Century egg')) Game.upgradesToRebuild=1;}, 10000);

		// utilise un setTimeout dedans car permet de "ralentir" le plugin quand le CPU est très demandé
		CCPG.self.mainLoop();

		// drôle
		// if(Game.baseSeason=="fools") {
			var jour = new Date();
			if(jour.getDate()==1 && jour.getMonth()==3) {	// grandma poisson d'avril
				CCPT.addClass(CCPG.game, "elderWrath");
				setTimeout(function() {CCPT.removeClass(CCPG.game, "elderWrath");}, 10000);
			}
		// }
	};

	/*=====================================================================================
	VERSIONS
	=======================================================================================*/
	this.checkVersion = function(verbal) {
		CCPT.ajax({
			url: this.versionSrc,
			get: {c: Date.now()},
			success: function(reponse) {
				if(+(reponse)>CCPG.self.version) {
					var titre = "Une nouvelle version de CCP est disponible !",
						desc = "Vous pouvez cliquer sur le bouton <b>\"recharger\"</b> des options pour mettre à jour votre version.",
						duree = 0;
					if(!verbal) {
						duree = CCPG.self.options.checkVersionRate.value/1000;
					}
					alert(titre, desc, [16,5], duree);
					if(this.options.checkVersionNotif.value) {
						CCPG.self.notifAdd(new CCPN(titre, desc, "checkVersionNotif"));
					}
				} else if(verbal) {
					alert("Aucune nouvelle version de CCP n'est disponible !", "Retentez votre chance !", [16,5]);
				}
			},
			error: function() {
				alert("<b>Erreur</b> : impossible de vérifier les mises à jour !", "", [11,6]);
			}
		});
	};
	this.checkVersionLoop = function() {
		clearInterval(this.checkVersionInterval);
		this.checkVersionInterval = setInterval(function() {if(CCPG.self.options.checkVersion.value) CCPG.self.checkVersion();}, this.options.checkVersionRate.value);
	};

	this.recharger = function() {
		location.href = "javascript:(function(d,s){s=d.body.appendChild(d.createElement('script'));s.setAttribute('title','CCPScript');s.setAttribute('src','"+this.bootstrapSrc+"?c='+Date.now());})(document)";
	};

	this.killGraphisme = function() {
		clearTimeout(this.mainLoopTimeout);

		CCPT.unbind(CCPG.gameButtons, "click", this.cliqueGameButtons);
		l("sectionLeft").removeEventListener('mouseenter', this.boutonWrinklersIn);
		l("sectionLeft").removeEventListener('mouseleave', this.boutonWrinklersOut);

		this.cliquePanelX();

		CCPT.remove(CCPG.CCPPanel);
		CCPT.remove(CCPG.CCPPanelButton);
		CCPT.remove(l('CCPPopWrinklers'));
		CCPT.remove(this.cache.CCPFlash);

		if(CCPDebug) {
			l("goldenCookie").style.border="";
			l("seasonPopup").style.border="";
		}

		CCPG.CCPPanel = undefined;
		CCPG.CCPPanelButton = undefined;
	};
	this.chargerGraphisme = function() {
		this.creerFlash();
		this.creerPanel();
		this.boutonWrinklers();

		CCPG.self.mainLoop();
	};
	this.rechargerGraphisme = function() {
		this.killGraphisme();
		this.chargerGraphisme();
	};

	this.kill = function() {
		clearTimeout(this.mainLoopTimeout);
		clearInterval(this.checkVersionInterval);
		// clearInterval(this.centuryEggInterval);
		CCPG.cron.stop();

		try {
			if(typeof _kill === "function") _kill();
		} catch(ex) {warn("Erreur Addon CCP : "+ex);}

		CCPT.unbind(CCPG.gameButtons, "click", this.cliqueGameButtons);
		l("sectionLeft").removeEventListener('mouseenter', this.boutonWrinklersIn);
		l("sectionLeft").removeEventListener('mouseleave', this.boutonWrinklersOut);
		document.removeEventListener('mousemove', this.corrigerMouseY);

		this.cliquePanelX();

		CCPT.removeClass(this.cache.productPrice, "text-green text-yellow text-orange text-red");

		CCPT.remove(CCPG.CCPPanel);
		CCPT.remove(CCPG.CCPPanelButton);
		CCPT.remove(l('CCPPopWrinklers'));
		CCPT.remove(this.cache.CCPFlash);

		CCPT.removeClass(document.body, "cleanUI");
		CCPT.removeClass(document.body, "boutonCCPFixe");
		if(CCPDebug) {
			l("goldenCookie").style.border="";
			l("seasonPopup").style.border="";
		}

		for(var nom in this.backup) {	// remet Game à son état "d'origine"
			var fct=this.backup[nom];	// eslint-disable-line no-unused-vars
			eval(nom+" = fct;");
			// window[nom] = fct;	// ne fonctionne qu'avec des "nom" simple (sans . ou sans [])
		}

		Game.RebuildUpgrades();

		// utilisation de "delete" obligatoire pour supprimer une propriété
		for(var i in CCPG.cache.CCPElem) {
			delete CCPG.cache.CCPElem[i].CCPProp;
		}

		// ne pas utiliser "delete" car ne fonctionne que pour les variables globales ou les objects et peut être lent
		CCPGlobals = undefined;
		CCPTools = undefined;

		CCPSaisonDrops = undefined;
		CCPSD = undefined;

		CCPOParamButton = undefined;
		CCPOParamRange = undefined;
		CCPOParamInputNum = undefined;
		CCPOParamInputStr = undefined;
		CCPOParamInputAud = undefined;
		CCPOParamButtonSub = undefined;
		CCPOParamButtonLink = undefined;
		CCPOParamInputPwd = undefined;
		CCPOption = undefined;
		CCPCron = undefined;

		whenGameReady = undefined;
		CCPDebug = undefined;

		CCPN = undefined;
		CCP = undefined;
		CCPO = undefined;
		CCPT = undefined;
		CCPG = undefined;

		// supprime les éventuels Addons
		if(typeof _init !== "undefined") _init = undefined;
		if(typeof _CCPO !== "undefined") _CCPO = undefined;
		if(typeof _creerPanel !== "undefined") _creerPanel = undefined;
		if(typeof _refreshPanel !== "undefined") _refreshPanel = undefined;
		if(typeof _mainLoop !== "undefined") _mainLoop = undefined;
		if(typeof _kill !== "undefined") _kill = undefined;

		// supprime les anciens scripts
		document.querySelectorAll("link[title=CCPStyle], script[title=CCPScript]").forEach(function(elem) {elem.parentNode.removeChild(elem);});
	};
}


// ------------------------------------

function CCPSaisonDrops(name, arr) {
	this.name = name || "";
	this.a = arr || [];
	this.m = this.a.length;
	this.n = 0;
	this.text = "0 / "+this.m;

	// ajoute l'option à l'object des saisons drops (CCPSD)
	if(!CCPSD.prototype[this.name]) {
		CCPSD.prototype[this.name] = this;
	}
}
function CCPSD() {
	new CCPSaisonDrops("halloween", ['Skull cookies','Ghost cookies','Bat cookies','Slime cookies','Pumpkin cookies','Eyeball cookies','Spider cookies']);
	new CCPSaisonDrops("noel", ['Christmas tree biscuits','Snowflake biscuits','Snowman biscuits','Holly biscuits','Candy cane biscuits','Bell biscuits','Present biscuits']);
	new CCPSaisonDrops("valentin", ['Pure heart biscuits','Ardent heart biscuits','Sour heart biscuits','Weeping heart biscuits','Golden heart biscuits','Eternal heart biscuits']);
	new CCPSaisonDrops("egg", Game.eggDrops);
	new CCPSaisonDrops("rareEgg", Game.rareEggDrops);
	new CCPSaisonDrops("garden", ['Elderwort biscuits','Bakeberry cookies','Duketater cookies','Green yeast digestives','Fern tea','Ichor syrup','Wheat slims']);
}

// ------------------------------------

function CCPOParamButton(text, callback, label) {
	this.on = text+" ON";
	this.off = text+" OFF";
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamRange(min, max, pas, callback, label) {
	this.min = min || "";
	this.max = max || "";
	this.pas = pas || "";
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamInputNum(callback, label) {
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamInputStr(callback, label) {
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamInputAud(ajouer, callback, label) {
	this.ajouer = ajouer || "";
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamButtonSub(text, callback, label) {
	this.text = text || "";
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamButtonLink(text, href, callback, label) {
	this.text = text || "";
	this.href = href || "";
	this.callback = callback || "";
	this.label = label || "";
}
function CCPOParamInputPwd(callback, label) {
	this.callback = callback || "";
	this.label = label || "";
}

function CCPOption(name, value, menu, params) {
	this.name = name;
	this.value = value;
	this.menu = menu || "";
	this.params = params || {};
	this.type = "";
	// http://stackoverflow.com/questions/6665997/switch-statement-for-greater-than-less-than
	if(this.params instanceof CCPOParamButton) {
		this.type = "button";
	} else if(this.params instanceof CCPOParamRange) {
		this.type = "range";
	} else if(this.params instanceof CCPOParamInputNum) {
		this.type = "inputNum";
	} else if(this.params instanceof CCPOParamInputStr) {
		this.type = "inputStr";
	} else if(this.params instanceof CCPOParamInputAud) {
		this.type = "inputAud";
	} else if(this.params instanceof CCPOParamButtonSub) {
		this.type = "buttonSub";
	} else if(this.params instanceof CCPOParamButtonLink) {
		this.type = "buttonLink";
	} else if(this.params instanceof CCPOParamInputPwd) {
		this.type = "inputPwd";
	}
	this.update = function(value) {
		if(value!==undefined) {
			this.value = value;
			Game.prefs[this.name] = value;
		} else {
			this.value = Game.prefs[this.name];
		}

		switch(this.type) {
			case "button" :
				var bouton = l(this.name);
				if(this.value) {
					bouton.innerHTML = this.params.on;
					CCPT.removeClass(bouton, "off");
				} else {
					bouton.innerHTML = this.params.off;
					CCPT.addClass(bouton, "off");
				}
			break;
			case "range" :
				l(this.name).value = this.value;
				l("txt"+this.name).innerHTML = CCPG.self.formatTime(this.value/1000, false, true);
			break;
			case "inputNum" :
				l(this.name).value = this.value;
			break;
			case "inputStr" :
			case "inputAud" :
			case "inputPwd" :
				l(this.name).value = this.value;
			break;
		}

		// if(typeof this.params.callback == "function") {
			// this.params.callback();
		// } else {
		if(this.params.callback) {
			eval(this.params.callback);
		}
	};

	Game.prefs[this.name] = this.value;

	// ajoute l'option à l'object des options (CCPO)
	if(!CCPO.prototype[this.name]) {
		CCPO.prototype[this.name] = this;
	}

	// return this;
}

// object pour gérer les options
function CCPO() {
	new CCPOption("infosObject", 0, "Bâtiments", new CCPOParamButton("Infos bâtiments","","(affiche des infos supplémentaires dans les tooltips des bâtiments)"));
	new CCPOption("colorPrices", 0, "Bâtiments", new CCPOParamButton("Prix bâtiments en couleur","CCPG.self.colorPrices();","(affiche des couleurs en fonction du BCI des bâtiments)"));
	new CCPOption("deficitLuck", 0, "Bâtiments", new CCPOParamButton("Lucky déficite","","(affiche si l'achat d'un bâtiment va créer un déficite sur les nombres d'or)"));
	new CCPOption("cookieDore", 0, "Cookies", new CCPOParamButton("Cookies Dorés","CCPT.hide(CCPG.self.cache.CCPStatNextCDDiv)","(affiche le temps maximum restant avant le prochain cookie doré)"));
	new CCPOption("cookieFlash", 0, "Cookies", new CCPOParamButton("Flash les Cookies Dorés","","(fait flasher l'écran lorsqu'un cookie doré apparait)"));
	new CCPOption("cookieAudio", 0, "Cookies", new CCPOParamButton("Son Cookies Dorés","","(joue un son lorsqu'un cookie doré apparait)"));
	new CCPOption("cookieAudioSrc", "", "Cookies", new CCPOParamInputAud("CCPG.self.jouerAudio()","CCPG.self.initAudios()","Lien d'un autre son pour les cookies dorés"));
	new CCPOption("cookieNotif", 0, "Cookies", new CCPOParamButton("Notification Cookies Dorés","CCPG.self.notifGrant()","(affiche une notification sur le bureau lorsqu'un cookie doré apparait)"));
	new CCPOption("cookieAlerteRed", 0, "Cookies", new CCPOParamButton("Aucune alerte rouge","","(ne fait flasher, jouer un son, afficher une notification que si le cookie doré n'est pas rouge)"));
	new CCPOption("cookieClicNotif", 0, "Cookies", new CCPOParamButton("Notification clique de Cookie","","(affiche une notification sur le bureau lorsque l'on clique sur un cookie doré)"));
	new CCPOption("saisonPopup", 0, "Rennes", new CCPOParamButton("Rennes","CCPT.hide(CCPG.self.cache.CCPStatNextSPDiv)","(affiche le temps maximum restant avant le prochain renne)"));
	new CCPOption("saisonFlash", 0, "Rennes", new CCPOParamButton("Flash les Rennes","","(fait flasher l'écran lorsqu'un renne apparait)"));
	new CCPOption("saisonAudio", 0, "Rennes", new CCPOParamButton("Son Rennes","","(joue un son lorsqu'un renne apparait)"));
	new CCPOption("saisonAudioSrc", "", "Rennes", new CCPOParamInputAud("CCPG.self.jouerAudio(true)","CCPG.self.initAudios()","Lien d'un autre son pour les rennes"));
	new CCPOption("saisonNotif", 0, "Rennes", new CCPOParamButton("Notification Rennes","CCPG.self.notifGrant()","(affiche une notification sur le bureau lorsqu'un renne apparait)"));
	new CCPOption("infosUpgrade", 0, "Upgrades", new CCPOParamButton("Infos upgrades","","(affiche des infos supplémentaires dans les tooltips des upgrades)"));
	new CCPOption("achatUpgradesClick", 0, "Upgrades", new CCPOParamButtonSub("Tout acheter","CCPG.self.achatUpgrades()","(achète tous les upgrades disponible dans le store et qui entrent dans le budget)"));
	new CCPOption("popWrinklers", 0, "Wrinklers", new CCPOParamButton("Larves","","(affiche un bouton pour tuer toutes les larves d'un coup)"));
	new CCPOption("preserveShiny", 0, "Wrinklers", new CCPOParamButton("Préserver le Shiny","","(ne pas tuer la larve blanche quand elle est là)"));
	new CCPOption("recolterMature", 0, "Minigames", new CCPOParamButtonSub("Récolter","CCPG.self.recolterMature()","(récolte toutes les plantes qui sont matures)"));
	new CCPOption("cleanUI", 0, "Divers", new CCPOParamButton("Clean UI","CCPG.self.cleanUI();","(nettoie l'affichage afin d'enlever le surplus, et le rendre plus joli)"));
	new CCPOption("boutonCCPFixe", 0, "Divers", new CCPOParamButton("Bouton CCP Fixe","CCPG.self.boutonCCPFixe();","(permet d'avoir le bouton CCP toujours à l'écran même quand on scroll)"));
	new CCPOption("refreshRate", 1000, "Divers", new CCPOParamRange(1000,5000,1000,"","Rafraîchissement toutes les"));
	new CCPOption("xPrestige", 0, "Divers", new CCPOParamInputNum("CCPG.self.cache.CCPXPrestige.value = CCPG.self.options.xPrestige.value","Choisir une valeur par défaut dans le prestige"));
	new CCPOption("checkVersion", 1, "Divers", new CCPOParamButton("Vérifier MAJ","","(vérifie s'il y a une mise à jour de CCP)"));
	new CCPOption("checkVersionNotif", 0, "Divers", new CCPOParamButton("Notification MAJ","CCPG.self.notifGrant()","(affiche une notification sur le bureau lorsqu'il y a une nouvelle mise à jour)"));
	// new CCPOption("checkVersionRate", 1800000, "Divers", new CCPOParamRange(900000,14400000,900000,"CCPG.self.checkVersionLoop()","Vérifie les mises à jour toutes les"));
	new CCPOption("checkVersionRate", 1800000, "Divers", new CCPOParamRange(300000,3600000,300000,"CCPG.self.checkVersionLoop()","Vérifie les mises à jour toutes les"));
	new CCPOption("checkVersionClick", 0, "Divers", new CCPOParamButtonSub("Vérifier la version","CCPG.self.checkVersion(true)","(vérifie si une nouvelle version est sortie en un clique)"));
	try {
		if(typeof _CCPO === "function") {
			_CCPO();
		}
	} catch(ex) {warn("Erreur Addon CCP : "+ex);}
}

// ------------------------------------

function CCPN(titre, body, tag) {
	this.titre = titre || "";
	this.body = body || "";
	this.tag = tag || "";
	// this.lang = lang || "";
	// this.icon = icon || "";
}

// ------------------------------------

/*
Cron simpliste avec des jokers (*)

 ┌───────── minute (0 - 59)
 │ ┌─────── heure (0 - 23)
 │ │ ┌───── jour du mois (1 - 31)
 │ │ │ ┌─── mois (1 - 12)
 │ │ │ │ ┌─ jour de la semaine (0 - 6) (dimanche=0)
 * * * * *
*/
function CCPCron() {
	this.id = 0;
	this.jobsid = 0;
	this.jobs = [];
	this.check = function() {
		var now = new Date();
		for(var i in this.jobs) {
			if((this.jobs[i].min=="*" || +this.jobs[i].min==now.getMinutes())
			&& (this.jobs[i].heu=="*" || +this.jobs[i].heu==now.getHours())
			&& (this.jobs[i].jou=="*" || +this.jobs[i].jou==now.getDate())
			&& (this.jobs[i].moi=="*" || +this.jobs[i].moi==(now.getMonth()+1))
			&& (this.jobs[i].jse=="*" || +this.jobs[i].jse==now.getDay())) {
				this.jobs[i].run();
			}
		}
	};
	this.start = function() {
		this.stop();
		this.id = setInterval(function() {CCPG.cron.check();}, 60000);
	};
	this.stop = function() {
		clearInterval(this.id);
	};
	this.add = function(fct, tmin, theu, tjou, jmoi, djse) {
		if(typeof fct === 'function') {
			var job = {
				id: this.jobsid,
				run: fct,
				min: +tmin || '*',
				heu: +theu || '*',
				jou: +tjou || '*',
				moi: +jmoi || '*',
				jse: +djse || '*',
			};
			this.jobs.push(job);
			this.jobsid++;
			return job.id;
		}
	};
	this.del = function(jid) {
		var index = this.jobs.findIndex(function(me) {
			return me.id === jid;
		});
		if(index!==-1) {
			this.jobs.splice(index, 1);	// économie de mémoire
		}
	};
	this.timeToCron = function(fct, t) {
		var time	= t | 0,	// OU binaire qui force en int, bien plus rapide
			days	= (time/86400 | 0) % 31,
			hours	= (time/3600 | 0) % 24,
			minutes	= (time/60 | 0) % 60;
		return this.add(fct, minutes, hours, days);
	};
}

// ------------------------------------

function CCPTools() {
	/**
	 * paramètres :
	 * {
	 *   url: string        une url sans paramètres
	 *   async: true/false  échange asynchrone ou synchrone (défaut: asynchrone)
	 *   get: array         tous les paramètres en GET
	 *   post: array        tous les paramètres en POST
	 *   success: function  callback en cas de réussite
	 *   error: function    callback en cas d'echec
	 * }
	 */
	this.ajax = function(jsonParam) {
		var url = jsonParam.url || '',
			async = (typeof jsonParam.async === 'undefined'?true:!!jsonParam.async),
			paramsGet = jsonParam.get || null,
			paramsPost = jsonParam.post || null,
			callback = jsonParam.success || null,
			callbackE = jsonParam.error || null;

		var xhr = (window.ActiveXObject?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest());
		if(xhr) {
			if(callback && typeof callback === 'function') {
				xhr.onreadystatechange = function() {
					if(xhr.readyState==4) {
						if(xhr.status==200) {
							callback(eval("("+xhr.responseText+")"));
						} else if(callbackE && typeof callbackE === 'function') {
							callbackE(xhr);
						}
					}
				};
			} else {
				throw new TypeError("callback is not a function");
			}
			if(paramsPost) {
				xhr.open('POST', url+(paramsGet?'?'+this._arrayToURI(paramsGet):''), async);
				xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xhr.send(this._arrayToURI(paramsPost));
			} else {
				xhr.open('GET', url+(paramsGet?'?'+this._arrayToURI(paramsGet):''), async);
				xhr.send(null);
			}
			return true;
		}
		return false;
	};
	this._arrayToURI = function(data) {
		return (typeof data=='string' ? data : Object.keys(data).map(
			function(k) {return k+'='+encodeURIComponent(data[k]);}
		).join('&'));
	};

	// array
	this.maxArray = function(ar) {return Math.max.apply(null, ar);};
	this.minArray = function(ar) {return Math.min.apply(null, ar);};

	// class
	this.hasClass = function(elem, className) {
		return elem && (elem.className || elem).toString().split(/\s+/).indexOf(className) > -1;
	};
	this.addClass = function(elem, classNames) {
		if(elem.length) {
			for(var i in elem) {
				this.addClass(elem[i], classNames);
			}
		} else {
			// elem = elem[0] || elem;
			if(elem.nodeType==1) {
				var arrClass = (classNames || "").split(/\s+/);
				for(var i in arrClass) {
					if(!this.hasClass(elem.className, arrClass[i])) {
						elem.className+=(elem.className?" ":"")+arrClass[i];
					}
				}
			}
		}
	};
	this.removeClass = function(elem, classNames) {
		if(elem.length) {
			for(var i in elem) {
				this.removeClass(elem[i], classNames);
			}
		} else {
			// elem = elem[0] || elem;
			if(elem.nodeType==1) {
				elem.className = (classNames!==undefined?
					elem.className.split(/\s+/).filter(function(className) {
						return !CCPT.hasClass(classNames, className);
					}).join(" ")
					:"");
			}
		}
	};

	// data
	this.data = function(elem, name, value) {
		var key = this._key(elem),
			cp = CCPG.cache.CCPProp;
		if(!cp[key]) {
			cp[key] = {};
		}
		if(value!==undefined) {
			cp[key][name] = value;
			return value;
		} else {
			return cp[key][name];
		}
	};
	this._duid = 1;
	this._key = function(elem) {
		var key = elem.CCPProp;
		if(!key) {
			key = this._duid++;
			Object.defineProperties(elem, {CCPProp: {value: key, configurable: true}});
			CCPG.cache.CCPElem.push(elem);
		}
		return key;
	};

	// display
	this.show = function(elem) {
		if(elem.length) {
			for(var i in elem) {
				this.show(elem[i]);
			}
		} else {
			if(elem.style) {
				elem.style.display = this.data(elem, "olddisplay") || "";

				if(this.css(elem, "display")==="none") {
					var display = this._defaultDisplay(elem);
					this.data(elem, "olddisplay", display);
					elem.style.display = display;
				}
			}
		}
	};
	this.hide = function(elem) {
		if(elem.length) {
			for(var i in elem) {
				this.hide(elem[i]);
			}
		} else {
			if(elem.style) {
				var old = this.data(elem, "olddisplay");
				if(!old || old==="none") {
					this.data(elem, "olddisplay", this.css(elem, "display"));
				}
				elem.style.display = "none";
			}
		}
	};
	this.visible = function(elem) {
		elem = elem[0] || elem;
		return this.css(elem, "display") !== 'none';
	};
	this.css = function(elem, name, value) {
		if(value!==undefined) {
			if(elem.style) {
				elem.style[name] = value;
				return elem.style[name];
			}
		} else {
			if(elem.style && elem.style[name]) {
				return elem.style[name];
			} else if(getComputedStyle) {
				var compStyle = getComputedStyle(elem);
				if(compStyle) {
					return compStyle[name];
				}
			}
		}
	};
	this._defaultDisplay = function(elem) {
		var display = this.css(elem, "display");
		if(display==="none") {
			var tag = elem.tagName,
				ct = CCPG.cache.CCPCSS;
			if(ct[tag]) {
				display = ct[tag];
			} else {
				var tmp = document.createElement(tag);
				document.body.append(tmp);

				display = this.css(tmp, "display");
				if(display==="none") {
					display = "block";
				}
				ct[tag] = display;

				tmp.parentNode.removeChild(tmp);
			}
		}
		return display;
	};

	// opacity
	this.fadeIn = function(elem, duration) {
		if(this.css(elem, "display")==="none") {
			if(elem.style) {
				elem.style.opacity = 0;
				this.data(elem, "fadeDir", "in");

				duration = +duration || 2000;
				CCPT._setOpacityIn(elem, 10/duration);
			}
		}
	};
	this.fadeOut = function(elem, duration) {
		if(this.css(elem, "display")!=="none") {
			if(elem.style) {
				elem.style.opacity = 1;
				this.data(elem, "fadeDir", "out");

				duration = +duration || 2000;
				CCPT._setOpacityOut(elem, 10/duration);
			}
		}
	};
	this._setOpacityIn = function(el, step) {
		if(this.data(el, "fadeDir")=="in") {
			var op = +(el.style.opacity) + step;
			if(op<1) {
				el.style.opacity = op;
				setTimeout(function() {
					CCPT._setOpacityIn(el, step);
				}, 10);
			} else {
				this.show(el);
				el.style.opacity = "";
			}
		}
	};
	this._setOpacityOut = function(el, step) {
		if(this.data(el, "fadeDir")=="out") {
			var op = +(el.style.opacity) - step;
			if(op>0) {
				el.style.opacity = op;
				setTimeout(function() {
					CCPT._setOpacityOut(el, step);
				}, 10);
			} else {
				this.hide(el);
				el.style.opacity = "";
			}
		}
	};

	// dom
	this.create = function(tag) {
		return document.createElement(tag);
	};
	this.attr = function(elem, name, value) {
		if(elem.nodeType!==2 && elem.nodeType!==3 && elem.nodeType!==8) {
			if(typeof name === "object") {
				var ret = [];
				for(var n in name) {
					ret.push(this.attr(elem, n, name[n]));
				}
				return ret;
			} else {
				if(value!==undefined) {
					if(value===null) {
						this.removeAttr(elem, name);
					} else {
						elem.setAttribute(name, value);
						return value;
					}
				} else {
					return elem.getAttribute(name);
				}
			}
		}
	};
	this.removeAttr = function(elem, name) {
		if(elem.nodeType===1) {
			elem.removeAttribute(name);
		}
	};
	this.append = function(ancre, elem) {
		if(ancre.nodeType===1 || ancre.nodeType===9 || ancre.nodeType===11) {
			ancre.appendChild(elem);
		}
	};
	this.prepend = function(ancre, elem) {
		if(ancre.nodeType===1 || ancre.nodeType===9 || ancre.nodeType===11) {
			ancre.insertBefore(elem, ancre.firstChild);
		}
	};
	this.remove = function(elem) {
		if(elem.length) {
			for(var i in elem) {
				this.remove(elem[i]);
			}
		} else {
			// elem = elem[0] || elem;
			if(elem.parentNode) {
				elem.parentNode.removeChild(elem);
			}
		}
	};

	// event
	this.click = function(elem, fct) {
		this.bind(elem, "click", fct);
	};
	this.hover = function(elem, fctOver, fctOut) {
		this.bind(elem, "mouseenter", fctOver);
		this.bind(elem, "mouseleave", fctOut || fctOver);
	};
	this.bind = function(elem, name, fct) {
		if(elem.length) {
			for(var i in elem) {
				this.bind(elem[i], name, fct);
			}
		} else {
			elem.addEventListener(name, fct);
		}
	};
	this.unbind = function(elem, name, fct) {
		if(elem.length) {
			for(var i in elem) {
				this.unbind(elem[i], name, fct);
			}
		} else {
			elem.removeEventListener(name, fct);
		}
	};
}
var CCPT = new CCPTools();

// ------------------------------------
// POLYFILL

// IE
if(!Array.prototype.find) {
	Object.defineProperty(Array.prototype, 'find', {
		value: function(predicate) {
			return _find(predicate);
		}
	});
}

// IE
if(!Array.prototype.findIndex) {
	Object.defineProperty(Array.prototype, 'findIndex', {
		value: function(predicate) {
			return _find(predicate, true);
		}
	});
}

function _find(predicate, byIndex) {
	if(this == null) {
		throw new TypeError('"this" is null or not defined');
	}
	var o = Object(this);
	var len = o.length >>> 0;
	if(typeof predicate !== 'function') {
		throw new TypeError('predicate must be a function');
	}
	var thisArg = arguments[1];
	var k = 0;
	while(k < len) {
		var kValue = o[k];
		if(predicate.call(thisArg, kValue, k, o)) {
			return (byIndex?k:kValue);
		}
		k++;
	}
	return (kValue?-1:undefined);
}

// ------------------------------------

// On se lance dès qu'on est prêt
var whenGameReady = setInterval(function() {
	if('object' === typeof Game) {
		if(Game.ready) {
			clearInterval(whenGameReady);

			Game.vanilla=0;

			// débloque un succès
			addChecks(function() {Game.Win('Third-party');});

			new CCP().init();
		}
	}
}, 50);


/* ================================================

    CCP - A Cookie Clicker plugin

    Version: 5.0
    License: MIT
    Website: https://ccp.vyshorin.fr
    GitLab: https://gitlab.com/ccplugin/ccp
    Authors: Victor Gavoille
             Florent Colinet

================================================ */

(function() {
	if(typeof CCP !== 'undefined') {
		if (Game.prefs.popups) Game.Popup('Rechargement de CCP en cours');
		else Game.Notify('Rechargement de CCP en cours','','',3);
		CCPG.self.kill();
	}

	var version = '5.0',	// eslint-disable-line no-unused-vars
		docFrag = document.createDocumentFragment(),
		host = "https://ccp.vyshorin.fr/",
		deps = [{
				type:    'link',
				url:     host+'ccp-plugin.css',
				async:   true,
				nocache: true
			},{
				type:    'script',
				url:     host+'ccp-plugin.js',
				async:   false,
				nocache: true
			}];

	for(var i=0; i<deps.length; i++) {
		var dep = deps[i],
			el = document.createElement(dep.type),
			qp = dep.nocache?'?c='+Date.now():'';

		if(dep.type == 'link') {
			el.title = 'CCPStyle';
			el.rel = 'stylesheet';
			el.href = dep.url + qp;
		} else if(dep.type == 'script') {
			el.title = 'CCPScript';
			el.src = dep.url + qp;
		}

		el.async = dep.async;		// Set async for ordered parsing
		docFrag.appendChild(el);	// Append to document fragment
	}

	document.head.appendChild(docFrag);
})();

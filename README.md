# ![ccp-logo.png](https://gitlab.com/ccplugin/ccp/raw/master/ccp-logo.png) CCP (Cookie Clicker Plugin) #

## Parce que les autres sont lent à la détente. ##
------

Plugin pour le jeu Cookie Clicker, comme de nombreux autres, mais qui essaye de suivre le plus rapidement possible les mises à jour du jeu et d'être le plus léger que possible.

Nous avons aussi un [wiki](https://gitlab.com/ccplugin/ccp/wikis/Home) qui est en cours de construction.

------
## Comment intaller : ##

* Pour le charger, faire un favori avec pour adresse :

```javascript
javascript:(function(d,s){s=d.body.appendChild(d.createElement('script'));s.setAttribute('title','CCPScript');s.setAttribute('src','https://ccp.vyshorin.fr/?c='+Date.now());})(document)
```

* Pour le charger par user script :

[Installation](https://ccp.vyshorin.fr/ccp-grease.user.js)
ou
```javascript
// ==UserScript==
// @name        CCP
// @namespace   https://ccp.vyshorin.fr
// @description Cookie Clicker Plugin
// @version     6.0
// @include     http://orteil.dashnet.org/cookieclicker/
// @exclude     http://orteil.dashnet.org/cookieclicker/beta/
// @downloadURL https://ccp.vyshorin.fr/ccp-grease.user.js
// @updateURL   https://ccp.vyshorin.fr/ccp-grease.user.js
// @grant       none
// ==/UserScript==

(function(d,s){
	s=d.body.appendChild(d.createElement('script'));
	s.setAttribute('title','CCPScript');
	s.setAttribute('src','https://ccp.vyshorin.fr/?c='+Date.now());
})(document);
```

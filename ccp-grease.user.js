// ==UserScript==
// @name        CCP
// @namespace   https://ccp.vyshorin.fr
// @description Cookie Clicker Plugin
// @version     6.0
// @include     http://orteil.dashnet.org/cookieclicker/
// @exclude     http://orteil.dashnet.org/cookieclicker/beta/
// @downloadURL https://ccp.vyshorin.fr/ccp-grease.user.js
// @updateURL   https://ccp.vyshorin.fr/ccp-grease.user.js
// @grant       none
// ==/UserScript==

(function(d,s) {
	s=d.body.appendChild(d.createElement('script'));
	s.setAttribute('title','CCPScript');
	s.setAttribute('src','https://ccp.vyshorin.fr/?c='+Date.now());
})(document);
